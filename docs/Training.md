# Training

## Training of the network
To train the network the ```train.py``` script is used. It loads the data, applies some preprocessing, trains the network, and saves some control plots.
Everything of the network will be saved in one directory.
After one training there should be three directories: One containing the control plots ```ControlPlots``` showing the score/scores of the network of the test set as well as the evolution of the training/validation loss with the number of epochs, two containing information saved by Tensorboard, ```train``` and ```validation```.
Several files are saved as well: ```architecture.json``` the architecture of the model as saved by tensorflow, ```bestModel.h5``` the weights of the model with the best performance on the validation set, ```config.yaml``` the used config file, ```preds.npy``` (or different depending on your configuration) the predictions of the network on the test set, ```quantile.transformer``` the transformer object used for the preprocessing, ```weightsForLwtnn.h5``` the weights of the best model but in a slightly different format which is needed for the lwtnn conversion script.

### Settings on the command line
Several settings can be set on the command line overwriting the values given in the config file. The saved config file will have the settings specified on the command line:
| Short argument | Long argument | Description | Overwrites |
| --- | --- | --- | --- |
| -l | --logDir | Directory where output will be saved | logDir |
| -t | --task | Train a network or predict on a test set. Either ```train``` or ```predict``` | task |
| -f | --testFile | File containing the test set | inputs/test |
| -p | --predsName | Name that the predictions will be saved under. ```.npy``` is added automatically at the end. | predsName |
