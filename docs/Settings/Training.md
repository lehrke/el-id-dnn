# Training Settings

## No default values
Settings that need to be set, i.e. don't have a default value.
| Setting name | Description |
| ------------ | ----------- |
| logDir | Directory where all outputs specific to that training/network will be saved |
| inputs | Dict of files used for the training. Needs one file for train, val, and test |

## Default values
Other settings with default settings in ```el-id-dnn/new_el_id/Training/config/defaultTrainingConfig.yaml```

| Setting name | Description | Default |
| ------------ | ----------- | ------- |
| multiClass | Whether to do multinomial or binary classification | True |
| weightName | Name of the weights to be used saved in the file | weightIncClassWeights |
| batchSize | Batch size used during the training | 4096 |
| epochs | Number of epochs to train the model for | 100 |
| loss | Loss to be used during the training | categorical_crossentropy |
| metrics | List of metrics to be evaluated during the training. These are evaluated without weights applied | categorical_crossentropy |
| weightedMetrics | List of metrics to be evaluated during the training but with weights applied | categorical_crossentropy |
| optimizer | Optimizer to be used during the training. The name of the optimizer can be given via className and additional parameters via the config dictionary which is treated as kwargs in the python code | <pre lang="yaml">optimizer:<br>  className:<br>    adam<br>  config:<br>    lr:<br>      0.0001</pre> |
| task | train or predict to either train a model or only predict using an already trained model | train |
| verbose | Verbosity of the training in tensorflow. 2 gives output at the end of each epoch, 1 at the end of each batch (not recommended for running with a log file since it will print a lot to the log file) | 2 |
| inputVars | List of variables to be used in the training. If additional variables are added or the order is changed, the tool in athena needs to be adjusted. | (see below or in the actual file) |
| predsName | Name that the final predictions will be saved under | preds |
| modelParameters | Describe the architecture of the model. See table below | |
| additionalPreprocessing | Preprocessing applied to specific variables. See table below | |
| callbacks | Callbacks to run during the training. See table below | |

### modelParameters
| Setting name | Description | Default |
| ------------ | ----------- | ------- |
| architecture | List of nodes in each hidden layer. A last output layer is always added based on whether multiclass or binary classification is performed. For multiclass the output layer has six nodes with a softmax activation, binary classification one node with sigmoid activation | <pre lang="yaml">modelParameters:<br>  architecture:<br>      - 256<br>      - 256<br>      - 256<br>      - 256<br>      - 256</pre> |
| activation | Which activation function to use after each hidden layer | leakyrelu |
| regularization | Amount of l2 regularization used in each hidden layer | 0 |
| doBatchNorm | Whether to use batch normalisation after each activation layer or not | True |
| dropout | Dropout in each hidden layer | 0 |

### callbacks
| Setting name | Description | Default |
| ------------ | ----------- | ------- |
| earlyStopping | Number of epochs after which the training is stopped if the validation loss doesn't improve | 50 |
| lrSchedule | Scheduler of the learning rate. The name of the class can be given as className and additional arguments as a dict in config which will be treated as kwargs in python. The default scheduler is a cyclical learning rate following a triangular pattern starting at the lowest learning rate given by baseLr. The learning rate increases after each batch up to a learning rate given by increaseLr * baseLr reaching the maximum after nEpochs. trainStepsPreEpoch is calculated in the script to caluclate how many batches are processed per epoch | <pre lang="yaml">callbacks:<br>   lrSchedule:<br>     className:<br>       cyclicLR<br>     config:<br>       baseLr: 1e-5<br>       increaseLr: 50<br>       nEpochs: 10<br>       trainStepsPerEpoch: 0</pre> |

### additionalPreprocessing
Anything changing here will also need changes inside the tool in athena.
| Setting name | Description | Default |
| ------------ | ----------- | ------- |
| abseta | Use $`\|\eta\|`$ instead of $`\eta`$ | True |
| maskf3 | Set f3 to a default value of $`0.05`$ for $`\|\eta\| > 2.01`$ due to bad modelling at high $`\eta`$ | True |
| maskTRT | Set TRTPID variable to $`0.15`$ for electrons without a TRTPID varibale. This is determined by $`\|\eta\| > 2.01`$ and $`\|\mathrm{TRTPID}\| < 10^{-6}`$| True |

## Config file
Full default training config file

```yaml
multiClass:
  True
weightName:
  weightIncClassWeights

batchSize:
  4096
epochs:
  100

loss:
  categorical_crossentropy
metrics:
  - categorical_crossentropy
weightedMetrics:
  - categorical_crossentropy

modelParameters:
  architecture:
      - 256
      - 256
      - 256
      - 256
      - 256
  activation:
    leakyrelu
  regularization:
    0
  doBatchNorm:
    True
  dropout:
    0

optimizer:
  className:
    adam
  config:
    lr:
      0.0001

callbacks:
  earlyStopping: 50
  lrSchedule:
    className:
      cyclicLR
    config:
      baseLr: 1e-5
      increaseLr: 50
      nEpochs: 10
      trainStepsPerEpoch: 0

task:
  train
verbose:
  2

inputVars:
  - d0
  - d0Sig
  - TRTPID
  - EoverP
  - dPOverP
  - deltaEta1
  - deltaPhiRescaled2
  - nPixelHits
  - nSCTHits
  - Rhad1
  - Rhad
  - f1
  - f3
  - weta2
  - Rphi
  - Reta
  - Eratio
  - wtots1
  - et
  - eta

additionalPreprocessing:
  abseta:
    True
  maskf3:
    True
  maskTRT:
    True
predsName:
  preds
```
