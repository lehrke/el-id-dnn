# General

At the moment only one setting is set in the general config file, namely the names of variables. It is used to map the names saved in the ROOT ntuples and therefore then the h5 files to common names inside the framework. So if the variable et has a different name in the used ntuples one can change the value of the definition of et in this part and it will be taken into account in the whole framework. The defaults can be seen below or under ```el-id-dnn/new_el_id/CommonTools/config/defaultCommonConfig.yaml```.

## Config file
```yaml
# Names of different variables
VariableNames:
  # input variables for the training
  d0: "d0"
  d0Sig: "d0Sig"
  TRTPID: "TRTPID"
  TRTPIDRNN: "TRTPIDRNN"
  EoverP: "EoverP"
  dPOverP: "dPOverP"
  deltaEta1: "deltaEta1"
  deltaPhiRescaled2: "deltaPhiRescaled2"
  nPixelHits: "nPixelHits"
  nSCTHits: "nSCTHits"
  Rhad1: "Rhad1"
  Rhad: "Rhad"
  f1: "f1"
  f3: "f3"
  weta2: "weta2"
  Rphi: "Rphi"
  Reta: "Reta"
  Eratio: "Eratio"
  wtots1: "wtots1"
  et: "et"
  eta: "eta"

  # truth information
  iff: "iff"
  truthType: "truthType"
  truth: "truth"
  multiTruth: "multiTruth"

  # additional variables used as rectangular cuts
  ambiguityBit: "ambiguityBit"
  nBlayerCut: "LHBlayerCut"

  # LH working points to make comparison plots
  LHLoose: "LHLoose"
  LHMedium: "LHMedium"
  LHTight: "LHTight"

  # weight from pileup reweighting
  pileupWeight: "pileupWeight"
```
