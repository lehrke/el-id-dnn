# Preprocessing

## No default values
Settings that need to be set, i.e. don't have a default value.
| Setting name | Description | Affects scripts |
| ------------ | ----------- | --------------- |
| inputNtupleDirs | List of ntuple directories to be transformed to h5 files. Needs to be given a dict where the key is the name of the directory that will be created for the transformed files of this ntuple and the value is the directory with all files. | convertNtupToH5.py |
| treeName | Name of the TTree in the ROOT file to be converted. Once everyone uses ntuples of the new tag and probe frame can make it default to the default name there. | convertNtupToH5.py |
| outDirConversion | Directory where the transformed h5 files will be saved. Each key given in inputNtupleDirs will be saved in one directory inside this directory. | convertNtupToH5.py (output) <br> plotSelection.py (input) <br> downsampling.py (input) |
| outDir | Directory where plots and downsampled files will be saved. | plotSelection.py <br> downsampling.py <br> weighting.py |

## Default values
Other settings with default settings in ```el-id-dnn/new_el_id/Preprocessing/config/defaultPreprocessingConfig.yaml```.

| Setting name | Description | Default | Affects scripts |
| ------------ | ----------- | ------- | --------------- |
| outFileNameConversion | Part of name of the converted files | ntupConverted | convertNtupToH5.py <br> plotSelection.py <br> downsampling |
| splitVariable | Based on which variable the splitting in different subsets should be done. |evtNumber | convertNtupToH5.py |
| split | Defines how the splitting should be performed. Should give a dictionary with the keys defining the name of the sample and the value the percentage of the full sample that this set should contain. The splitting is done using modulo of the splitVariable | <pre lang="yaml">split: <br>  train: 50<br>  val: 25<br>  test: 25</pre> | convertNtupToH5.py |
| outFileName | Part of the name of the downsampled files | slimmed | downsampling.py <br> weighting.py |
| dataset | Name of dataset to be put onto plots | Zee+JF | plotSelection.py <br> downsampling.py <br> weighting.py |
| splitDownsampled | Number of single files the downsampled files will be split in | 10 | downsampling.py |
| plotVars | List of names of variables to plot | <pre lang="yaml">plotVars:<br>  -et<br>  -eta</pre> | plotSelection.py <br> downsampling.py <br> weighting.py|
| variables | All variables that should be plotted need to be defined here. It defines the binning to use, the name of the file the plot is saved in, the label on the x axis, whether to use log scale for x/y axis. Only the settings for $`E_\mathrm{T}`$ are shown here. For all see the file below or in the repository. In bins the binning is described with low being the lowest bin edge, high the highest, number the number of bins, and log whether a lograithmic scale should be used for the bins. The remaining settings are fileName which is added to the saved output, plotName which is the label for the x axis, and xlog/ylog which determine if the x/y axis is drawn on a log scale | <pre lang="yaml">et:<br>    bins:<br>      low: 4e3<br>      high: 2e6<br>      number: 100<br>      log: True<br>    fileName: Et<br>    plotName: $E_T$ [MeV]<br>    xlog: True<br>    ylog: True </pre> | plotSelection.py <br> downsampling.py <br> weighting.py |
| selectionNames | Name of the file to save the plots of the selected distributions and some text to add on the plot. | outName: selection <br> text: Selected Distributions | plotSelection.py |
| downsamplingNames | Same as selectionNames but for downsampling plots | outName: downsampled <br> text: Downsampled Distributions | downsampling.py |
| weightingNames | Same as selectionNames but for reweighting plots | outName: weighted <br> text: Downsampled + Weighted Distributions | weighting.py |
| classWeightingNames | Same as selectionNames but for reweighting plots including the class weights | outName: classWeighted <br> text: "Downsampled + Weighted Distributions\nIncluding Class Weights"
| classes | List of names of the classes to be used in the legend of the plots | Signal, CF, PC, HF, LFEg, LFH | plotSelection.py <br> downsampling.py <br> weighting.py |
| removeCFs | Set to true to remove all charge flip electrons during the downsampling | False | downsampling.py |
| downsamplingBins | Bins in $`E_\mathrm{T}`$ to be used to downsample the signal electrons. Type is either log or lin to use log spaced or linear spaced bins, low is the lowest bin edge, high the highest, nBins the number of bins, and in additional a list of additional bins which don't fit a uniform log/lin binning can be given. | <pre lang="yaml">downsamplingBins:<br>  type: log<br>  low: 4<br>  high: 600<br>  nBins: 100<br>  additional:<br>    - 5000</pre> | downsampling.py |
| downsamplingFactor | Number of signal electrons in given downsamplingbins kept after downsampling is given by downsamplingFactor times number of background electrons in this bin. If there are less signal electrons, all are kept. | 5 | downsampling.py |
| weightingBinsET | Bins in $`E_\mathrm{T}`$ used for the reweighting. The structure is the same as for downsamplingBins | <pre lang="yaml">weightingBinsET:<br>  type: log<br>  low: 4<br>  high: 400<br>  nBins: 99<br>  additional:<br>    - 2000</pre> | weighting.py |
| weightingHistET | Values of target distribution in the defined bins of the weighting operation in $`E_\mathrm{T}`$. | (too many numbers, see file in repository) | weighting.py |
| weightingBinsAbsEta | Bins in $`\|\eta\|`$ used for the reweighting. The structure is the same as for downsamplingBins | <pre lang="yaml">weightingBinsAbsEta:<br>  type: lin<br>  low: 0<br>  high: 2.47<br>  nBins: 49</pre> | weighting.py |

## Config file
Full default preprocessing config file:
```yaml
outFileNameConversion:
  ntupConverted

splitVariable:
  evtNumber

split:
  train: 50
  val: 25
  test: 25


outFileName:
  slimmed

dataset:
  Zee + JF17

splitDownsampled:
  10
plotVars:
  - et
  - eta

variables:
  et:
    bins:
      low: 4e3
      high: 2e6
      number: 100
      log: True
    fileName: Et
    plotName: $E_T$ [MeV]
    xlog: True
    ylog: True
  eta:
    bins:
      low: -2.47
      high: 2.47
      number: 100
      log: False
    fileName: Eta
    plotName: $\eta$
    xlog: False
    ylog: True

selectionNames:
  outName: selection
  text: Selected Distributions
downsamplingNames:
  outName: downsampled
  text: Downsampled Distributions
weightingNames:
  outName: weighted
  text: Downsampled + Weighted Distributions
classWeightingNames:
  outName: classWeighted
  text: "Downsampled + Weighted Distributions\nIncluding Class Weights"

classes:
  - Signal
  - CF
  - PC
  - HF
  - LFEg
  - LFH

removeCFs:
  False

downsamplingBins:
  type: log
  low: 4
  high: 600
  nBins: 100
  additional:
    - 5000

downsamplingFactor:
  5


weightingBinsET:
  type: log
  low: 4
  high: 400
  nBins: 99
  additional:
    - 2000

# too many values to show here, see actual file
# weightingHistET:

# Reweighting in |eta| is using a flat distribution as target
weightingBinsAbsEta:
  type: lin
  low: 0
  high: 2.47
  nBins: 49
```
