# Installation
This part is based on the documentation of the [Umami](https://umami-docs.web.cern.ch/setup/installation/) framework for flavour tagging.
It is recommended to use the framework with a Docker image.

## Docker container
The easiest way is to use the framework in a Docker container. It has all dependencies and the package itself installed. During the CI a Docker image is created.

### Launching containers using Docker (local machine)
On a local machine with docker installed, you can launch the framework container with this command
```
docker run --rm -it docker://gitlab-registry.cern.ch/lehrke/el-id-dnn/train-image
```

### Launching containers using Singularity (lxplus/institute cluster)
On most institute clusters as well as on ```lxplus``` you don't have access to Docker, but instead you can use singularity. You can find information on how to use singularity on ```lxplus``` [here](https://hsf-training.github.io/hsf-training-docker/10-singularity/index.html) though it is not recommended to use ```lxplus```. To launch a container with singularity you can run
```
singularity exec --nv docker://gitlab-registry.cern.ch/lehrke/el-id-dnn/train-image bash
```
which will open a shell within the container. The ```--nv``` makes sure that you can access GPUs within the container. To include directories other than the home directory you can mount them inside the container using ```--bind /eos``` or similar. To speed up this process you can also build a singularity file of the container which you can then use
```
singularity build image.simg docker://gitlab-registry.cern.ch/lehrke/el-id-dnn/train-image bash
singularity exec --nv image.simg bash
```

### Modifying code
To modify code within the framework and run with the modified code you first need a local copy of the framework by cloning the repository via
```
git clone ssh://git@gitlab.cern.ch:7999/lehrke/el-id-dnn.git
```
If you are now in a docker container you can install the package via
```
python -m pip install -e .
```
If this leads to an error message Singularity might be configured such that it is not writable. In that case run
```
export PYTHONPATH=$PWD:$PYTHONPATH
python -m pip install --prefix python_install -e .
```
This will install the package in ```python_install```.
The directory needs to be created beforehand.
To now run with the modified code you need to point to that directory, so in the base directory of the repo ```python_install/bin/train.py``` to run the training script.
This will create a two directories ```new_el_id.egg-info``` and ```python_install``` which should not be committed to git. They are included in ```.gitignore``` so as long as the name isn't changed during installation, there is nothing to worry about.

### Local installation using virtual environments
If you don't want to use docker/singularity you can also use a virtual python environement.
Create a virtual environment with python ```3.6``` or higher. The docker image uses ```3.6``` at the moment so ```3.6``` would be preferred.
Inside the virtual environment run
```
pip install -r requirements.txt
python -m pip install -e .
```
