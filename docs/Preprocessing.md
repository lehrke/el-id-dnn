# Preprocessing

## Conversion to hdf5
The first step is to convert the ROOT ntuples to a file format more commonly used in machine learning, hdf5. This is done with the `convertNtupToH5.py` script. It will create output files for each process defined, splitting the ntuple into a training, validation, and testing set. Additionally, hdf5 files with virtual datasets pointing to the single process files are created. These are usually the files that one should interact with.

## Plot variables from the transformed hdf5 files (optional)
This step is optional but always good to check if the hdf5 files contain data that looks reasonable. To run use the `plotSelection.py` script.

## Downsampling
Since the samples of the single classes that are used don't have the same distribution in $`E_\mathrm{T}`$ and $`|\eta|`$, using the original distributions would lead to a large bias with respect to these variables. Therefore, a mixture of downsampling and reweighting is applied. First, the `downsampling.py` script removes some signal electrons based on et and the amount of signal and background electrons available. This is done so that the weights needed to match the distributions are kept at a reasonably low range. Plots of the downsampled distributions are created.

## Reweighting
To fully match the distributions, the distributions of the single classes are separately reweighted in $`E_\mathrm{T}`$ and $`|\eta|`$, multiplying the two weights to obtain a final weight.
The final weight is saved in the downsampled h5 files keeping the mean of weights of the single classes equal to one.
Using these weights during the training would again lead to a bias, since the network see much more signal electrons than e.g. electrons from heavy-flavour decays.
Therefore, an additional weight is saved combining the previously mentioned weight and a class weight which scaled all classes to have the same sum of weights.
Since the reweighting is done separately in $`E_\mathrm{T}`$ and $`|\eta|`$, small differences of the distributions with the weights applied can be seen.
If the weighting was already run and therefore a weight is already saved, a warning will be printed and only plots of the reweighted distributions will be made.
