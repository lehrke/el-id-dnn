# General Usage
The framework is split into several parts which can be further split into several scripts to easily repeat single steps such as the training of the network without running the whole chain again.
If the repository is installed as a python package (see [Installation](Installation.md) for more info), all necessary scripts and configuration files are installed, meaning you can run them from anywhere.
Each script will read a common default configuration file and a default configuration file specific to the part of the framework.

## Config Files

Each script needs to be given one config file on the command line.
This is used to set some settings that are needed because a default value doesn't make sense to be given (e.g. paths to files/directories).
Default settings can also be overwritten within this config file.
Both, default settings specific to that part of the framework and general default settings can be modified within this config file.
One can also give two config files, one overwriting the settings specific to the part of the framework and one overwriting the general default settings.

### Command line arguments
Some scripts support overwriting certain settings of the configuration files on the command line.
