import h5py
import numpy as np
import argparse

from new_el_id.CommonTools import CustomParser, mkdir
from new_el_id.ConversionToAthena import ConversionConfiguration


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = CustomParser(description="??")

    args = parser.parse_args()

    return args


def writeEtaCuts(confFile, comment: str, cutName: str, cutValue: int) -> None:
    confFile.write(f"\n# {comment}\n{cutName}: ")
    for i in range(9):
        confFile.write(f"{cutValue}; ")
    confFile.write(f"{cutValue}\n")


def writeEtEtaCuts(
    confFile, comment: str, valueName: str, values: np.ndarray, commentsLine: str
) -> None:
    valuesShape = values.shape
    confFile.write(f"\n# {comment}\n")
    for i in range(valuesShape[0]):
        if i == 0:
            confFile.write(f"{valueName}: ")
        else:
            confFile.write(f"+{valueName}: ")
        for j in range(valuesShape[1]):
            if i != valuesShape[0] - 1 or j != valuesShape[1] - 1:
                confFile.write(f"{values[i][j]:.5f}; ")
            else:
                confFile.write(f"{values[i][j]:.5f} ")

        confFile.write(f"# {commentsLine[i]} #\n")


if __name__ == "__main__":
    args = getParser()
    configLoader = ConversionConfiguration(args.configFile)
    configLoader.replaceWithCommandLineArguments(args)
    configLoader.loadTrainingConfig()
    config = configLoader.config
    variableList = configLoader.trainConfig["inputVars"]
    multiClass = configLoader.trainConfig["multiClass"]

    rangeEnergies = [
        "<7 GeV",
        "7-10 GeV",
        "10-15 GeV",
        "15-20 GeV",
        "20-25 GeV",
        "25-30 GeV",
        "30-35 GeV",
        "35-40 GeV",
        "40-45 GeV",
        ">45 GeV",
    ]
    rangeFractions = [
        "|eta| 0.0-0.1",
        "|eta| 0.1-0.6",
        "|eta| 0.6-0.8",
        "|eta| 0.8-1.15",
        "|eta| 1.15-1.37",
        "|eta| 1.37-1.52",
        "|eta| 1.52-1.81",
        "|eta| 1.81-2.01",
        "|eta| 2.01-2.37",
        "|eta| 2.37-2.47",
    ]

    if config["cfSignal"]:
        fractions = np.load(config["networkLogDir"] + "/optimizedFractionsCFSignal.npy")
    else:
        fractions = np.load(config["networkLogDir"] + "/optimizedFractions.npy")

    for wp in config["workingPoints"]:
        thresholds = np.load(config["saveDir"] + f"/dump/{wp}Thresholds.npy")
        with open(
            config["saveDir"] + f"ElectronDNNMulticlass{wp}.conf", "w"
        ) as confFile:
            confFile.write(f"inputModelFileName: {config['networkFileName']}\n")
            confFile.write(f"inputQuantileFileName: {config['QTFileName']}\n")

            confFile.write("\n# Variables used in the MVA tool, Comma separated\n")
            confFile.write("Variables: ")
            nVars = len(variableList)
            for i in range(nVars):
                # remove p_ which is currently used in hdf5 files
                if i < nVars - 1:
                    confFile.write(f"{variableList[i]},")
                else:
                    confFile.write(f"{variableList[i]}\n")

            writeEtEtaCuts(
                confFile,
                "cut on ML score. Must be 10 (columns) x 10 (rows) long",
                "CutSelector",
                thresholds,
                rangeEnergies,
            )

            writeEtaCuts(
                confFile,
                "cut on ambiguity. Must be 10 long",
                "CutAmbiguity",
                config["tuningValues"][wp]["cutAmbBit"],
            )
            writeEtaCuts(
                confFile,
                "cut on pixel-layer hits. Must be 10 long",
                "CutPi",
                config["tuningValues"][wp]["cutPi"],
            )
            writeEtaCuts(
                confFile,
                "cut on SCT hits. Must be 10 long",
                "CutSCT",
                config["tuningValues"][wp]["cutSCT"],
            )
            writeEtaCuts(
                confFile,
                "cut on precision hits. Must be 10 long",
                "CutBL",
                config["tuningValues"][wp]["cutBL"],
            )

            confFile.write(
                "\n# interpolate cut value along et\ndoSmoothBinInterpolation: TRUE\n"
            )

            if multiClass:
                confFile.write(
                    f"\nmultiClass: TRUE\ncfSignal: {'TRUE' if config['cfSignal'] else 'FALSE'}"
                )
                writeEtEtaCuts(
                    confFile,
                    "fractions to combine single output nodes. Must be 5 (columns) x 10 (rows) long",
                    "Fractions",
                    fractions,
                    rangeFractions,
                )
