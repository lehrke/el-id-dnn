from new_el_id.CommonTools import Configuration


class ConversionConfiguration(Configuration):
    def __init__(self, config: str):
        super(ConversionConfiguration, self).__init__(config, conversion=True)

    def loadTrainingConfig(self) -> None:
        self.trainConfig = self.loadConfig(
            self.config["networkLogDir"] + "/config.yaml"
        )
