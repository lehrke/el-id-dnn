"""Calculate thresholds on DNN preds to match sig eff of a LH working point."""
import h5py
import numpy as np
import argparse

from new_el_id.CommonTools import CustomParser, mkdir
from new_el_id.ConversionToAthena import ConversionConfiguration


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = CustomParser(description="??")

    args = parser.parse_args()

    return args


class Predictions:
    def __init__(
        self, fileName: str, label: str, fractionFile: str, cfSignal: bool = True
    ):
        self.fileName = fileName
        self.nodes = np.load(fileName)
        self.preds = self.nodes
        self.label = label
        self.fractionFile = fractionFile
        if self.fractionFile:
            self.fractions = np.load(fractionFile)
        self.cfSignal = cfSignal

    def getPredictionsWithCut(self, mask: np.ndarray):
        p = copy.deepcopy(self.preds)
        p[mask == 1] = -999.0
        return self.preds[mask == 1]

    def applyCutOnPredictions(self, mask: np.ndarray):
        self.preds[mask == 1] = -999.0

    def buildDiscriminant(self, eta: np.ndarray, etaBins: np.ndarray):
        if len(self.nodes.shape) == 1:
            self.preds = self.nodes
            return
        elif hasattr(self, "fractions"):
            self.preds = np.zeros(shape=(self.nodes.shape[0]))
            for k in range(len(etaBins) - 1):
                kinMask = (np.abs(eta) < etaBins[k + 1]) & (np.abs(eta) > etaBins[k])

                self.preds[kinMask == 1] = self.buildBinDiscriminant(
                    self.fractions[k], self.nodes[kinMask == 1]
                )
        else:
            print("no fractions found for multiclass model. Using default fractions")
            fracs = np.array([0.01, 0.01, 0.01, 0.17, 0.8])
            self.preds = np.zeros(shape=(self.nodes.shape[0]))
            self.preds = self.buildBinDiscriminant(fracs, self.nodes)

    def buildBinDiscriminant(self, fracs: np.ndarray, preds: np.ndarray):
        if self.cfSignal:
            return ((1 - fracs[0]) * preds[:, 0] + fracs[0] * preds[:, 1]) / (
                fracs[1] * preds[:, 2]
                + fracs[2] * preds[:, 3]
                + fracs[3] * preds[:, 4]
                + fracs[4] * preds[:, 5]
            )
        else:
            return preds[:, 0](
                fracs[0] * preds[:, 1]
                + fracs[1] * preds[:, 2]
                + fracs[2] * preds[:, 3]
                + fracs[3] * preds[:, 4]
                + fracs[4] * preds[:, 5]
            )


def calculateThresholds(config: dict, wp: str) -> None:
    """Calculate thresholds and print them out at the end."""
    # Load data
    print(config["VariableNames"])
    with h5py.File(config["testFile"], "r") as hf:
        et = hf[config["VariableNames"]["et"]][:] / 1000.0
        eta = hf[config["VariableNames"]["eta"]][:]
        iff = hf[config["VariableNames"]["iff"]][:]
        ambType = hf[config["VariableNames"]["ambiguityBit"]][:]
        blHits = hf[config["VariableNames"]["nBlayerCut"]][:]

    p = Predictions(
        config["networkLogDir"] + "/" + config["predsName"] + ".npy",
        "doesn't matter",
        config["networkLogDir"] + "/optimizedFractionsCFSignal.npy",
        cfSignal=config["cfSignal"],
    )

    mc = len(p.preds.shape) > 1
    if mc:
        p.buildDiscriminant(
            eta, np.array([0, 0.1, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37, 2.47])
        )
        preds = p.preds
        preds = np.log(preds[iff == 2])
    else:
        preds = -np.log(1.0 / preds[iff == 2] - 1) / 10.0

    # Mask all bkg since we're only interested in signal efficiency
    et = et[iff == 2]
    eta = eta[iff == 2]

    ambType = ambType[iff == 2]
    blHits = blHits[iff == 2]

    preds[ambType >= config["tuningValues"][wp]["cutAmbType"]] = -999
    preds[blHits < config["tuningValues"][wp]["cutBL"]] = -999

    # Et/eta bins in which the thresholds are determined
    etBins = [4, 7, 10, 15, 20, 25, 30, 35, 40, 45, 6000]
    etaBins = [0.0, 0.1, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37, 2.47]

    thresholds = np.ones(shape=(10, 10))

    sigEffArray = np.array(config["tuningValues"][wp]["sigEff"])
    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            # Mask data not in the Et/eta bin
            mask = (
                (et > etBins[i])
                & (et <= etBins[i + 1])
                & (np.abs(eta) >= etaBins[j])
                & (np.abs(eta) < etaBins[j + 1])
            )

            # Make array containing desired signal efficiencies
            sigEff = sigEffArray[i, j]

            # Determine threshold needed to achieve desired sig eff
            # TODO right now using multiclass preds for binary by just using signal node !!
            sortedPreds = -np.sort(-preds[mask == 1])
            # Workaround for now if not all bins have data
            if len(sortedPreds) == 0:
                thresholds[i, j] = -999
            else:
                thresholds[i, j] = sortedPreds[int(np.sum(mask) * sigEff)]

    mkdir(config["saveDir"])
    mkdir(config["saveDir"] + "/dump")

    np.save(config["saveDir"] + f"/dump/{wp}Thresholds.npy", thresholds)


if __name__ == "__main__":
    args = getParser()
    configLoader = ConversionConfiguration(args.configFile)
    configLoader.replaceWithCommandLineArguments(args)
    config = configLoader.config

    for wp in config["workingPoints"]:
        calculateThresholds(config, wp)
