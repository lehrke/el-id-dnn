"""Transform a saved QuantileTransformer into a root file as used in athena."""
import uproot3 as uproot
import joblib
import argparse

from new_el_id.CommonTools import CustomParser, mkdir
from new_el_id.ConversionToAthena import ConversionConfiguration


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = CustomParser(description="??")

    parser.add_argument(
        "-n",
        "--networkLogDir",
        action="store",
        type=str,
        help="Directory where QuantileTransformer is stored.",
    )
    parser.add_argument(
        "-s",
        "--saveDir",
        action="store",
        type=str,
        help="Directory where ROOT file will be saved",
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = getParser()
    configLoader = ConversionConfiguration(args.configFile)
    configLoader.replaceWithCommandLineArguments(args)
    configLoader.loadTrainingConfig()
    config = configLoader.config
    variableList = configLoader.trainConfig["inputVars"]

    mkdir(config["saveDir"])

    qt = joblib.load(config["networkLogDir"] + "/quantile.transformer")

    ref = qt.references_
    quant = qt.quantiles_

    treeDict = {"references": "double"}
    treeExtendDict = {"references": ref}

    for i, var in enumerate(variableList):
        # at the moment variables are saved as p_* in hdf5 files but we want
        # just * in the root file
        treeDict[var] = "double"
        treeExtendDict[var] = quant[:, i]

    with uproot.recreate(config["saveDir"] + "/" + config["QTFileName"]) as f:
        f["tree"] = uproot.newtree(treeDict)

        f["tree"].extend(treeExtendDict)
