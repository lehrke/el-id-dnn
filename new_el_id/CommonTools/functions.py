"""Function used throughout the whole framework."""
import numpy as np
import os
from sklearn.preprocessing import QuantileTransformer
import matplotlib.pyplot as plt


def mkdir(directory: str) -> None:
    """
    Create a directory at the given path.

    Args
    ----
        directory: *str*
            Path where the directory should be created. If the directory
            already exists, nothing happens.
    """
    if not os.path.exists(directory):
        os.makedirs(directory)


def weightedQuantileTransformer(
    data: np.ndarray, qtReferences: np.ndarray, weights: np.ndarray
) -> QuantileTransformer:
    """
    Create a quantile transformer taking weights into account.

    Args
    ----
        data : *np.ndarray*
            Array of the input data.
        qtReferences : *np.ndarray*
            Array which contains the values of the desired quantiles
        weights : *np.ndarray*
            Weights of the distributions

    Return
    ------
        q : sklearn.preprocessing.QuantileTransformer
            QuantileTransformer object which was fit manually taking weights
            into account.

    """
    # Create QuantileTransformer object and set reference_ to the given ones
    q = QuantileTransformer()
    q.references_ = qtReferences
    # Create "empty" quantiles to fill
    quantiles = np.zeros((qtReferences.shape[0], data.shape[1]))

    # Process each of the features separately
    for i in range(data.shape[1]):
        # Sort the data and the weights
        ind = np.argsort(data[:, i])
        sortedData = data[ind, i]
        sortedWeights = weights[ind]
        # sortedWeights = np.clip(sortedWeights, 0, 1)
        # The first weight has to be 0 for some reason
        # (not completely sure why exactly)
        sortedWeights[0] = 0

        # Interpolate using the qtReferences as wanted x-coordinates, the
        # normalized cumsum of the weights as x-values, and the data as
        # y-values
        # seems like numpy's cumsum does some large approximation so we have
        # to calculate it ourselves
        cumsumme = np.zeros(sortedWeights.shape[0])
        for j in range(cumsumme.shape[0]):
            cumsumme[j] = cumsumme[j - 1] + sortedWeights[j]
        quantiles[:, i] = np.interp(qtReferences, cumsumme / cumsumme[-1], sortedData)

    q.quantiles_ = quantiles
    return q


def positionLegend(fig, ax) -> None:
    """
    Extend y-axis range such that legend can be placed w/o overlap with plot.

    Args
    ----
        fig: *matplotlib figure*
            Figure object which the legend should be placed on.
        ax: *matplotlib axis*
            Axis object which will hold the legend.

    """
    # Split legend into columns such that there are never more than 3 rows
    ncol = int(np.floor(len(ax.get_legend_handles_labels()[1]) / 3 + 0.9))
    # Place the legend in the upper right to be able to get the position to
    # adjust the axis range
    leg = ax.legend(loc="upper right", ncol=ncol)
    # Draw the plot to make it possible to extract actual positions.
    plt.draw()
    # Get legend positions in pixels of the figures
    a = leg.get_frame().get_bbox().bounds
    ylow, yup = ax.get_ylim()
    # Transform the figure pixels to data coordinates
    q = ax.transData.inverted().transform((0, a[1]))
    # Claculate the new upper limit of the y-axis depending on whether the plot
    # is on a logarithmic scale or not
    if ax.get_yscale() == "log":
        newYup = 10 ** (
            (
                np.log10(q[1]) * np.log10(ylow)
                + np.log10(yup) ** 2
                - 2 * np.log10(ylow) * np.log10(yup)
            )
            / (np.log10(q[1]) - np.log10(ylow))
        )
    else:
        newYup = (q[1] * ylow + yup ** 2 - 2 * ylow * yup) / (q[1] - ylow)

    # Set the new axis limits
    ax.set_ylim([ylow, newYup])


def plotHistogram(
    data: np.ndarray,
    mask: list,
    labels: list,
    weights: np.ndarray,
    bins: np.ndarray,
    outputName: str,
    xlog: bool = False,
    ylog: bool = False,
    xlabel: str = None,
    text: str = None,
) -> None:
    """
    Plot a histogram.

    Args
    ----
        data: *np.ndarray*
            Array of data which will be plotted in the histogram.
        mask: *list*
            List of masks to plot different components of the data, e.g.
            different bkg classes.
        labels: *list*
            Labels for the different components specicified by the different
            masks.
        weights: *np.ndarray*
            Weights to be used when creating the histogram.
        bins: *np.ndarray*
            Bins of the histogram.
        outputName: *str*
            Name where the histogram will be saved under.
        xlog: *bool* = False
            Use a logarithmic scale for the x-axis.
        ylog: *bool* = False
            Use a logarithmic scale for the y-axis.
        xlabel: *str* = None
            Label for the x-axis.
        text: *str* = None
            Additional text placed on the plot next to the legend.
    """
    fig, ax = plt.subplots()
    if weights is None:
        weights = np.ones(data.shape)

    for i in range(len(mask)):
        ax.hist(
            data[mask[i]],
            bins=bins,
            histtype="step",
            weights=weights[mask[i]],
            label=labels[i],
        )

    if xlog:
        ax.set_xscale("log")
    if ylog:
        ax.set_yscale("log")
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if text is not None:
        ax.text(0.02, 0.98, text, transform=ax.transAxes, verticalalignment="top")
    positionLegend(fig, ax)
    plt.tight_layout()

    plt.savefig(outputName)
    plt.close("all")


rcParams = {
    "font.size": 30,
    "axes.titlesize": 44,
    "axes.labelsize": 40,
    "axes.grid": True,
    "grid.alpha": 0.4,
    "lines.linewidth": 4,
    "lines.markersize": 20,
    "lines.antialiased": False,
    "patch.linewidth": 4,
    "xtick.labelsize": 36,
    "ytick.labelsize": 36,
    "xtick.top": True,
    "xtick.direction": "in",
    "xtick.minor.visible": True,
    "xtick.major.size": 5,
    "ytick.right": True,
    "ytick.direction": "in",
    "ytick.minor.visible": True,
    "ytick.major.size": 5,
    "legend.fontsize": 36,
    "legend.columnspacing": 0.5,
    "legend.handletextpad": 0.4,
    # taken from PyATLASstyle
    "axes.unicode_minus": False,
    "pdf.fonttype": 42,
    "figure.figsize": [19.20, 10.80],
    "figure.titlesize": 44,
    "figure.subplot.hspace": 0.0,
    "figure.subplot.wspace": 0.1,
}
