import yaml
import re
import pkg_resources
import argparse
import h5py
import numpy as np


class CustomParser(argparse.ArgumentParser):
    """Base Class for an ArgumentParser used througout the framework.

    Mos scripts need a configFile passed to the ArgumentParser. This class
    creates an ArgumentParser with one positional argument holding this
    configFile.
    """

    def __init__(self, **kwargs):
        """
        Initialize the CustomParser.
        """
        super(CustomParser, self).__init__(**kwargs)

        self.add_argument(
            "configFile", action="store", type=str, help="Config file to read from."
        )
        self.add_argument(
            "-c",
            "--commonConfigFile",
            action="store",
            type=str,
            default=None,
            help="Custom common config file to read from.",
        )


class Configuration:
    """Base Class for reading a .yaml configuration file and preparing it for use."""

    def __init__(
        self,
        configFile: str,
        commonConfigFile: str = None,
        preprocessing: bool = False,
        training: bool = False,
        plotting: bool = False,
        optimization: bool = False,
        conversion: bool = False,
    ):
        """
        Initializate the Configuration base class.

        Args
        ----
            configFile: *str*
                Path where the configuration file is saved.
            commonConfigFile: *str* = None
                Path where a configuration file with settings for the whole
                framework are set. Does not have to be set.
            preprocessing: *bool* = False
                Whether the configuration is used for preprocessing. Will load
                defaultPreprocessingConfig if so.
            training: *bool* = False
                Whether the configuration is used for training. Will load
                defaultTrainingConfig if so.
            plotting: *bool* = False
                Whether the configuration is used for plotting. Will load
                defaultPlottingConfig if so.
            optimization: *bool* = False
                Whether the configuration is used for optimization. Will load
                no additional config if so.
            conversion: *bool* = False
                Whether the configuration is used for conversion to athena. Will load
                defaultConversionConfig if so.
        """
        self.configFile = configFile
        self.commonConfigFile = commonConfigFile
        self.loader = self.getLoader()
        self.config = self.loadConfig(self.configFile)
        self.defaultCommonConfig = yaml.load(
            pkg_resources.resource_stream(
                "new_el_id.CommonTools", "config/defaultCommonConfig.yaml"
            ),
            Loader=self.loader,
        )
        if self.commonConfigFile is not None:
            self.commonConfig = self.loadConfig(self.commonConfigFile)

        if preprocessing:
            self.defaultConfig = yaml.load(
                pkg_resources.resource_stream(
                    "new_el_id.Preprocessing", "config/defaultPreprocessingConfig.yaml"
                ),
                Loader=self.loader,
            )
        elif training:
            self.defaultConfig = yaml.load(
                pkg_resources.resource_stream(
                    "new_el_id.Training", "config/defaultTrainingConfig.yaml"
                ),
                Loader=self.loader,
            )
        elif plotting:
            self.defaultConfig = yaml.load(
                pkg_resources.resource_stream(
                    "new_el_id.Plotting", "config/defaultPlottingConfig.yaml"
                ),
                Loader=self.loader,
            )
        elif optimization:
            pass
        elif conversion:
            self.defaultConfig = yaml.load(
                pkg_resources.resource_stream(
                    "new_el_id.ConversionToAthena",
                    "config/defaultConversionConfig.yaml",
                ),
                Loader=self.loader,
            )
        else:
            pass

        if hasattr(self, "defaultConfig"):
            self.config = self.mergeConfigs(self.config, self.defaultConfig)

        if hasattr(self, "commonConfig"):
            self.config = self.mergeConfigs(self.config, self.commonConfig)

        self.config = self.mergeConfigs(self.config, self.defaultCommonConfig)

    def getLoader(self):
        """Create a yaml loader that can read scientific notification like 1e-3."""
        loader = yaml.SafeLoader
        # This is needed to load numbers in scientific notation
        loader.add_implicit_resolver(
            "tag:yaml.org,2002:float",
            re.compile(
                """^(?:
             [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
            |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
            |\\.[0-9_]+(?:[eE][-+][0-9]+)?
            |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
            |[-+]?\\.(?:inf|Inf|INF)
            |\\.(?:nan|NaN|NAN))$""",
                re.X,
            ),
            list("-+0123456789."),
        )

        return loader

    def loadConfig(self, configFile: str) -> dict:
        """
        Read the config file.

        Args
        ----
            configFile: *str*
                Path to the config file to read.
        """
        with open(configFile, "r") as ymlfile:
            cfg = yaml.load(ymlfile, Loader=self.loader)

        return cfg

    def replaceWithCommandLineArguments(self, args: argparse.Namespace) -> None:
        """
        Replace items in the configuration with arguments set on the command line.

        Args
        ----
            args: *argparse.Namespace*
                Arguments returned from an ArgumentParser. The arguments set on
                the command line have priority over values set in the config file.
        """
        args = vars(args)

        for key in args:
            if args[key] is not None:
                self.config[key] = args[key]

    def mergeConfigs(self, a, b, path=None):
        "merges b into a"
        if path is None:
            path = []
        for key in b:
            if key in a:
                if isinstance(a[key], dict) and isinstance(b[key], dict):
                    self.mergeConfigs(a[key], b[key], path + [str(key)])
                else:
                    pass
            else:
                a[key] = b[key]
        return a

    def dump(self, fileName: str) -> None:
        """
        Save the config to disk.

        Args
        ----
            fileName: *str*
                Path where the config will be saved.
        """
        with open(fileName, "w") as yamlFile:
            yaml.dump(self.config, yamlFile, default_flow_style=False)


class PlottingBase:
    """Base Class for plotting which is mainly for loading data."""

    def __init__(self, config: dict, fileName: str, weights: str):
        """
        Initialize PlottingBase class.

        Args
        ----
            config: *dict*
                Dictionary with all necessary configurations for the plotting.
            fileName: *str*
                File where the data for the plotting is saved.
            weights: *str*
                Name of variable in the file which holds the weights which
                should be used for plotting. If no weights should be used, pass
                None.
        """
        self.config = config
        self.outDir = self.config["outDir"]
        self.fileName = fileName
        self.weights = weights

    def _loadTruthInfo(self) -> None:
        """Load the truth information of the file."""
        # TODO save a variable in the files that gives the multiclass labels directly
        with h5py.File(self.fileName, "r") as hf:
            self.truth = hf[f"{self.config['VariableNames']['truth']}"][:]
            self.multiTruth = hf[f"{self.config['VariableNames']['multiTruth']}"][:]

    def _loadWeights(self) -> None:
        """
        Load the weights from the file if weights is specified in initialization
        otherwise create weight array with weight equal one for all events.
        """
        with h5py.File(self.fileName, "r") as hf:
            if self.weights:
                self.weights = hf[self.weights][:]
            else:
                self.weights = np.ones(self.truth.shape)

    def _loadVariable(self, var: str, newDict: bool = False) -> None:
        """
        Load one variable at a time from disk.

        Args
        ----
            var: *str*
                Name of the variable to load.
            newDict: *bool* = False
                Whether the variable should be loaded into an existing dictionary
                if multiple variables are needed for the plotting or if it should
                override an potentially already existing dictionary with another
                variable saved. This is useful to reduce memory usage.
        """
        if newDict or not hasattr(self, "data"):
            self.data = {}

        with h5py.File(self.fileName, "r") as hf:
            self.data[var] = hf[self.config["VariableNames"][var]][:]
