from scipy.optimize import minimize
import numpy as np
import h5py
from sklearn.metrics import roc_curve
from new_el_id.CommonTools import Configuration, CustomParser
import argparse


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = CustomParser(description="Options for NN Training")

    parser.add_argument(
        "--networkLogDir",
        action="store",
        type=str,
        help="Directory where outputs will be saved.",
    )
    parser.add_argument(
        "--testFile",
        action="store",
        type=str,
        help="File where test data will be " + "loaded from.",
    )
    parser.add_argument("--predsName", action="store", type=str, help="??")

    args = parser.parse_args()

    return args


def aucWindow(
    preds: np.ndarray,
    truth: np.ndarray,
    weights: np.ndarray = None,
    low: float = 0.60,
    up: float = 0.98,
) -> float:
    """
    Calculate the AUC of the ROC curve of predictions.

    Args
    ----

        ???

    Returns
    -------
        aucLowUp : *float*
            Area Under the Curve for the roc curve in the window betweeen low
            and up.

    """
    if truth.shape[0] == 0:
        return -999
    # Calculate the fpr and tpr
    fpr, tpr, _ = roc_curve(truth, preds, sample_weight=weights)
    # Interpolate the fpr to get the value at the low/up end of the window
    fpLow = np.interp(low, tpr, fpr)
    fpUp = np.interp(up, tpr, fpr)
    # Add the fpr at the low/up end of the window and the values of the edges
    tpr = np.append(tpr, [low, up])
    fpr = np.append(fpr, [fpLow, fpUp])
    # Mask to only have fpr/tpr in the desired window
    fpr = fpr[(tpr <= up) & (tpr >= low)]
    tpr = tpr[(tpr <= up) & (tpr >= low)]
    # Sort the values so the tpr is rising
    idx = np.argsort(tpr)
    fpr = fpr[idx]
    tpr = tpr[idx]
    # Estimate the AUC using a trapezoidal integration.
    aucLowUp = np.trapz(fpr, tpr)
    return aucLowUp


def buildDiscriminant(
    preds: np.ndarray, fractions: np.ndarray, cfSignal: bool
) -> np.ndarray:
    if cfSignal:
        return ((1 - fractions[0]) * preds[:, 0] + fractions[0] * preds[:, 1]) / (
            fractions[1] * preds[:, 2]
            + fractions[2] * preds[:, 3]
            + fractions[3] * preds[:, 4]
            + fractions[4] * preds[:, 5]
        )
    else:
        return (preds[:, 0]) / (
            fractions[0] * preds[:, 1]
            + fractions[1] * preds[:, 2]
            + fractions[2] * preds[:, 3]
            + fractions[3] * preds[:, 4]
            + fractions[4] * preds[:, 5]
        )


def calcPartialAUC(
    fractions: np.ndarray,
    preds: np.ndarray,
    truth: np.ndarray,
    cfSignal: bool,
    masks: list,
) -> float:
    discriminant = buildDiscriminant(preds, fractions, cfSignal)

    scores = np.ones(shape=len(masks))
    for i in range(len(masks)):
        scores[i] = aucWindow(
            discriminant[masks[i] == 1], truth[masks[i] == 1], low=0.7, up=0.95
        )

    return np.sum(scores)


def evaluateGrid(
    fractionsToTest: np.ndarray,
    preds: np.ndarray,
    truth: np.ndarray,
    cfSignal: bool,
    masks: list,
) -> list:
    output = []
    for i in range(len(fractionsToTest)):
        output.append(calcPartialAUC(fractionsToTest[i], preds, truth, cfSignal, masks))

    return output


if __name__ == "__main__":
    args = getParser()
    configLoader = Configuration(args.configFile, optimization=True)
    configLoader.replaceWithCommandLineArguments(args)
    config = configLoader.config

    with h5py.File(config["testFile"], "r") as hf:
        et = hf[config["VariableNames"]["et"]][:] / 1e3
        eta = hf[config["VariableNames"]["eta"]][:]
        iff = hf[config["VariableNames"]["iff"]][:]

    particleMask = (iff == 2) | (iff == 5) | (iff > 7)
    truth = iff == 2
    etBins = np.array([4, 7, 10, 15, 20, 25, 30, 35, 40, 45, 2000])
    etaBins = np.array([0.0, 0.8, 1.37, 1.52, 2.01, 2.47])

    preds = np.load(config["networkLogDir"] + f"/{config['predsName']}.npy")

    if config["cfSignal"]:
        possibleFractions = np.linspace(0.01, 0.97, 24)
        allCombinations = np.array(
            np.meshgrid(
                possibleFractions[:6],
                possibleFractions[:6],
                possibleFractions[:6],
                possibleFractions,
                possibleFractions[12:],
            )
        ).T.reshape(-1, 5)
        fractionsToTest = allCombinations[np.sum(allCombinations[:, 1:], axis=1) == 1]
    else:
        possibleFractions = np.linspace(0.01, 0.96, 24)
        allCombinations = np.array(
            np.meshgrid(
                possibleFractions[:10],
                possibleFractions[:10],
                possibleFractions[:10],
                possibleFractions,
                possibleFractions[10:],
            )
        ).T.reshape(-1, 5)
        fractionsToTest = allCombinations[np.sum(allCombinations, axis=1) == 1]

    gridOutputs = []

    bnds = ((0.001, 0.999), (0.001, 1), (0.001, 1), (0.001, 1), (0.001, 1))

    optOutputsFracs = np.ones(shape=(len(etaBins) - 1, 10, 5))
    optOutputsVals = np.ones(shape=(len(etaBins) - 1, 10))
    bestFracs = np.ones(shape=(len(etaBins) - 1, 5))

    for i in range(len(etaBins) - 1):
        kinMask = (np.abs(eta) < etaBins[i + 1]) & (np.abs(eta) > etaBins[i])
        p = preds[(particleMask == 1) & (kinMask == 1)]
        t = truth[(particleMask == 1) & (kinMask == 1)]
        e = et[(particleMask == 1) & (kinMask == 1)]

        masks = []
        for j in range(len(etBins) - 1):
            masks.append((e > etBins[j]) & (e < etBins[j + 1]))

        gridOutputs.append(
            evaluateGrid(fractionsToTest, p, t, config["cfSignal"], masks)
        )

        minimumToOptimize = np.sort(gridOutputs[-1])[10]

        counter = 0
        for k in range(len(gridOutputs[-1])):
            if gridOutputs[-1][k] <= minimumToOptimize:
                if counter == 10:
                    break
                res = minimize(
                    calcPartialAUC,
                    fractionsToTest[k],
                    args=(p, t, config["cfSignal"], masks),
                    bounds=bnds,
                    options={"disp": False, "eps": 1e-1},
                )
                optOutputsFracs[i, counter] = res.x
                optOutputsVals[i, counter] = res.fun
                counter += 1
        bestFracs[i] = optOutputsFracs[i, np.argmin(optOutputsVals[i])]

    gridOutputs = np.array(gridOutputs)
    suf = "CFSignal" if config["cfSignal"] else ""

    fractionsToSave = np.ones(shape=(10, 5))
    if config["cfSignal"]:
        for i in range(len(etaBins) - 1):
            bestFracs[i, 1:] /= np.sum(bestFracs[i, 1:])
    else:
        for i in range(len(etaBins) - 1):
            bestFracs[i] /= np.sum(bestFracs[i])
    # Optimization is done in five eta bins but during application 10 eta bins are used at the moment
    fractionsToSave[0, :] = bestFracs[0, :]
    fractionsToSave[1, :] = bestFracs[0, :]
    fractionsToSave[2, :] = bestFracs[0, :]
    fractionsToSave[3, :] = bestFracs[1, :]
    fractionsToSave[4, :] = bestFracs[1, :]
    fractionsToSave[5, :] = bestFracs[2, :]
    fractionsToSave[6, :] = bestFracs[3, :]
    fractionsToSave[7, :] = bestFracs[3, :]
    fractionsToSave[8, :] = bestFracs[4, :]
    fractionsToSave[9, :] = bestFracs[4, :]

    np.save(
        config["networkLogDir"] + f"/allFractionsOptimized{suf}.npy", optOutputsFracs
    )
    np.save(
        config["networkLogDir"] + f"/allFractionsOptimizedValue{suf}.npy",
        optOutputsVals,
    )
    np.save(config["networkLogDir"] + f"/optimizedFractions{suf}.npy", fractionsToSave)
