"""Plot performance plot for several trained networks."""
import numpy as np
import h5py
import argparse
import copy
import json

from new_el_id.CommonTools import CustomParser, mkdir
from new_el_id.Plotting import PlottingConfiguration, ValidationPlotting


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = CustomParser(description="Options for evaluation of NN training")

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = getParser()

    configLoader = PlottingConfiguration(args.configFile)
    configLoader.replaceWithCommandLineArguments(args)
    configLoader.getPredictionAndFractionFiles()
    configLoader.processConfiguration()
    config = configLoader.config

    etBins = np.array([15, 20, 25, 30, 35, 40, 45, 60, 100])
    etaBins = np.array([0, 0.8, 1.37, 1.52, 2.01, 2.47])
    plotting = ValidationPlotting(config, etBins=etBins, etaBins=etaBins, weights=None)
    plotting.loadData()
    plotting.loadPredictions()
    plotting.plot()
