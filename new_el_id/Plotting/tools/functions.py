from new_el_id.CommonTools import Configuration, PlottingBase, mkdir
from new_el_id.Plotting.tools import (
    Predictions,
    plotRejectionPlots,
    plotBinnedROCCurve,
    plotBinnedROCCurveRatio,
)
import numpy as np
import copy
import os.path


class PlottingConfiguration(Configuration):
    def __init__(self, configFile: str):
        super(PlottingConfiguration, self).__init__(configFile, plotting=True)

    def getPredictionAndFractionFiles(self) -> None:
        if not "predictionFiles" in self.config:
            self.config["predictionFiles"] = [
                d + "/" + self.config["defaultPredictionName"] + ".npy"
                for d in self.config["networkDirs"]
            ]

        nPreds = len(self.config["predictionFiles"])

        if not "fractionFiles" in self.config:
            tmp = []
            for i in range(nPreds):
                if "networkDirs" not in self.config:
                    ffile = ""
                elif self.config["cfSignal"][i]:
                    ffile = (
                        self.config["networkDirs"][i]
                        + "/"
                        + self.config["defaultFractionName"]
                        + "CFSignal.npy"
                    )

                else:
                    ffile = (
                        self.config["networkDirs"][i]
                        + "/"
                        + self.config["defaultFractionName"]
                        + ".npy"
                    )

                if os.path.isfile(ffile):
                    tmp.append(ffile)
                else:
                    tmp.append(None)

            self.config["fractionFiles"] = tmp

    def processConfiguration(self) -> None:
        """
        Based on the arguments of the ArgParser, configure some variables.

        Args
        ----
            args: ??
                Arguments of the argument parser (see above).

        Returns
        -------
            ambCut: *list*
                List which is one if a cut on the ambiguity bit should be applied
                to the predictions corresponding to this position.
            BLrCur: *list*
                List which is one if a cut on the number of b-layer should be
                applied to the predictions corresponding to this position.
            dataSet: *str*
                String which will be put on plots to indicate which dataset was
                used for the evaluation.

        """
        # If only one value for ambiguityCut or nBlayerCut is given apply this value for all predictions
        if (
            self.config["ambiguityCut"] is not None
            and len(self.config["ambiguityCut"]) == 1
        ):
            self.config["ambiguityCut"] = self.config["ambiguityCut"] * len(
                self.config["predictionFiles"]
            )
        else:
            self.config["ambiguityCut"] = self.config["ambiguityCut"]
        if (
            self.config["nBlayerCut"] is not None
            and len(self.config["nBlayerCut"]) == 1
        ):
            self.config["nBlayerCut"] = self.config["nBlayerCut"] * len(
                self.config["predictionFiles"]
            )
        else:
            self.config["nBlayerCut"] = self.config["nBlayerCut"]

        # Use from common config...
        if self.config["dataSet"] == "ZJF":
            self.config["dataSet"] = r"$Z+\mathrm{JF17}$"
        elif self.config["dataSet"] == "Ztt":
            self.config["dataSet"] = r"$Z+t\bar{t}$"
        elif self.config["dataSet"] == "ZJFJpsi":
            self.config["dataSet"] = r"$Z+J/\psi+\mathrm{JF17}$"
        elif self.config["dataSet"] == "ZJFJpsiCF":
            self.config["dataSet"] = r"$Z+J/\psi+\mathrm{JF17}$ CF included"


class ValidationPlotting(PlottingBase):
    def __init__(
        self,
        config: dict,
        etBins: np.ndarray,
        etaBins: np.ndarray,
        weights: np.ndarray = None,
        scaleET: bool = True,
    ):
        super(ValidationPlotting, self).__init__(
            config=config, fileName=config["inputFile"], weights=weights
        )
        self.selection = False
        self.etBins = etBins
        self.etaBins = etaBins
        self.scaleET = scaleET

        mkdir(self.outDir + "/Plots")
        if self.config["plotROCCurves"]:
            mkdir(self.outDir + "/Plots/ROC")
        if len(self.config["plotRejectionPlots"]) > 0:
            mkdir(self.outDir + "/Plots/Rejection")

    def loadData(self) -> None:
        variables = [
            "LHLoose",
            "LHMedium",
            "LHTight",
            "et",
            "eta",
            "ambiguityBit",
            "nBlayerCut",
        ]
        for var in variables:
            self._loadVariable(var, newDict=False)

        if self.scaleET:
            self.data["et"] *= 1e-3
        self._loadTruthInfo()
        self._loadWeights()

    def loadPredictions(self) -> None:
        fracEtaBins = np.array(
            [0, 0.1, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37, 2.47]
        )

        self.preds = []
        for i in range(len(self.config["predictionFiles"])):
            p = Predictions(
                self.config["predictionFiles"][i],
                self.config["labels"][i],
                self.config["fractionFiles"][i],
                cfSignal=self.config["cfSignal"][i],
            )
            p.buildDiscriminant(self.data["eta"], fracEtaBins)
            self.preds.append(p)

    def plot(self) -> None:
        # if len(self.config["plotRejectionPlots"]) > 0:
        #     for point in self.config["plotRejectionPlots"]:
        #         plotRejectionPlots(
        #             self.config,
        #             self.config["outDir"] + "/Plots/Rejection/",
        #             self.truth,
        #             copy.deepcopy(self.data),
        #             copy.deepcopy(self.preds),
        #             self.etBins,
        #             self.etaBins,
        #             self.multiTruth != 1,
        #             workingPoint="LH" + point,
        #         )

        self.applyRectangularCuts()

        if self.config["plotBkgClassesSeparate"]:
            for i, bkgClass in enumerate(
                ["ChargeFlip", "PhotonConv", "HeavyFlavor", "LFEgamma", "LFHadron"]
            ):
                plotBinnedROCCurve(
                    self.config,
                    self.config["outDir"] + "/Plots/ROC/",
                    self.truth,
                    copy.deepcopy(self.data),
                    copy.deepcopy(self.preds),
                    self.etBins,
                    self.etaBins,
                    self.multiTruth == (i + 1),
                    bkgClass,
                    weights=None,  # self.data["p_pileupWeight"],
                    text="Evaluated on " + self.config["dataSet"],
                )
                # if len(self.preds) > 1:
                #     plotBinnedROCCurveRatio(
                #         self.config,
                #         self.config["outDir"] + "/Plots/ROC/",
                #         self.truth,
                #         copy.deepcopy(self.data),
                #         copy.deepcopy(self.preds),
                #         self.etBins,
                #         self.etaBins,
                #         self.multiTruth == (i + 1),
                #         bkgClass,
                #         weights=None, #self.data["p_pileupWeight"],
                #         text="Evaluated on " + self.config["dataSet"],
                #     )

        self.maskChargeFlips()

        plotBinnedROCCurve(
            self.config,
            self.config["outDir"] + "/Plots/ROC/",
            self.truth,
            self.data,
            self.preds,
            self.etBins,
            self.etaBins,
            weights=None,
            text="Evaluated on " + self.config["dataSet"],
        )
        # if len(self.preds) > 1:
        #     plotBinnedROCCurveRatio(
        #         self.config,
        #         self.config["outDir"] + "/Plots/ROC/",
        #         self.truth,
        #         self.data,
        #         self.preds,
        #         self.etBins,
        #         self.etaBins,
        #         weights=None,
        #         text="Evaluated on " + self.config["dataSet"],
        #     )

    def applyRectangularCuts(self) -> None:
        for i in range(len(self.preds)):
            mask = self.data["ambiguityBit"] >= self.config["ambiguityCut"][i]
            if self.config["nBlayerCut"][i] == 1:
                blrMask = self.data["nBlayerCut"] == 0
                mask = (mask == 1) & (blrMask == 1)
            self.preds[i].applyCutOnPredictions(mask)

    def maskChargeFlips(self) -> None:
        cfMask = self.multiTruth != 1
        for key in self.data:
            self.data[key] = self.data[key][cfMask == 1]
        for i in range(len(self.preds)):
            self.preds[i].preds = self.preds[i].preds[cfMask == 1]
        self.truth = self.truth[cfMask == 1]
