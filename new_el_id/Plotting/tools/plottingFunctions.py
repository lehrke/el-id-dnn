"""
Functions which are used for plotting.

Common arguments for the functions in this file. If there are specific things
for a function, they will be mentioned in the functions doc string.

logDir: *str*
    Directory where everything will be saved from this function. Often the
    actual directory where stuff will be saved is a subdirectory to keep plots
    more organized. When a single model should be plotted give the logDir of
    this model. For multiple models give a logDir which needs to be created
    before.

plotData: *dict*
    Dictionary containing all the necessary additional data for the respective
    function. Each key is the name of a variable and has an array as value
    holding this variable. Common needed variables are: et, eta, LH working
    points (e.g. LHLoose).

preds: *array* or *dict*
    Predictions of the model that should be plotted. If multiple models should
    be plotted on one plot (only possible/sensible for some functions) a
    dictionary should be passed which has one item for each model containing
    in 'preds' an array holding the predictions and in 'label' a string which
    will be used in the legend of the plots to distinguish between models. In
    the doc string of each function it is stated whether multiple models can be
    plotted with this function.

etBins: *array*
    Array containing the bin edges in Et which are used for the plots.
    E.g. [4, 15, 25, 40, 60, 5000] would result in 5 bins with the first one
    going from 4 to 15

etaBins: *array*
    Array containing the bin edges in eta which are used for the plots.
    Usually the absolute value of eta is used.
    E.g. [0.0, 1.32, 1.57, 2.47] would result in 3 bins with the first one
    going from 0.0 to 1.32

"""
import numpy as np
from sklearn.metrics import roc_curve
import copy
from scipy.interpolate import pchip

# Make sure that plotting works while running on a cluster
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt

from new_el_id.CommonTools import positionLegend, rcParams, mkdir

# Set some of the common parameters to make plots "nice" and hopefully readable
# if put into presentations.
plt.rcParams.update(rcParams)


class Predictions:
    def __init__(
        self, fileName: str, label: str, fractionFile: str, cfSignal: bool = True
    ):
        self.fileName = fileName
        self.nodes = np.load(fileName)
        self.preds = self.nodes
        self.label = label
        self.fractionFile = fractionFile
        if self.fractionFile:
            self.fractions = np.load(fractionFile)
        self.cfSignal = cfSignal

    def getPredictionsWithCut(self, mask: np.ndarray):
        p = copy.deepcopy(self.preds)
        p[mask == 1] = -999.0
        return self.preds[mask == 1]

    def applyCutOnPredictions(self, mask: np.ndarray):
        self.preds[mask == 1] = -999.0

    def buildDiscriminant(self, eta: np.ndarray, etaBins: np.ndarray):
        if len(self.nodes.shape) == 1:
            self.preds = self.nodes
            return
        elif hasattr(self, "fractions"):
            self.preds = np.zeros(shape=(self.nodes.shape[0]))
            for k in range(len(etaBins) - 1):
                kinMask = (np.abs(eta) < etaBins[k + 1]) & (np.abs(eta) > etaBins[k])

                self.preds[kinMask == 1] = self.buildBinDiscriminant(
                    self.fractions[k], self.nodes[kinMask == 1]
                )
        else:
            print("no fractions found for multiclass model. Using default fractions")
            fracs = np.array([0.01, 0.01, 0.01, 0.17, 0.8])
            self.preds = np.zeros(shape=(self.nodes.shape[0]))
            self.preds = self.buildBinDiscriminant(fracs, self.nodes)

    def buildBinDiscriminant(self, fracs: np.ndarray, preds: np.ndarray):
        if self.cfSignal:
            return ((1 - fracs[0]) * preds[:, 0] + fracs[0] * preds[:, 1]) / (
                fracs[1] * preds[:, 2]
                + fracs[2] * preds[:, 3]
                + fracs[3] * preds[:, 4]
                + fracs[4] * preds[:, 5]
            )
        else:
            return preds[:, 0](
                fracs[0] * preds[:, 1]
                + fracs[1] * preds[:, 2]
                + fracs[2] * preds[:, 3]
                + fracs[3] * preds[:, 4]
                + fracs[4] * preds[:, 5]
            )


class ValuesAndUncertainties:
    def __init__(self, values: np.ndarray, uncertainties: np.ndarray, label: str):
        self.values = values
        self.uncertainties = uncertainties
        self.label = label

    def getAxis(self, index: int, axis: int):
        if axis == 0:
            return ValuesAndUncertainties(
                self.values[index], self.uncertainties[index], self.label
            )
        elif axis == 1:
            return ValuesAndUncertainties(
                self.values[:, index], self.uncertainties[:, index], self.label
            )


def errorBarPlot(
    binCenters: np.ndarray,
    binLength: np.ndarray,
    vals: list,
    outName: str,
    xlabel: str = "",
    ylabel: str = "",
    text: str = "",
):
    fig, ax = plt.subplots()
    for v in vals:
        ax.errorbar(
            binCenters,
            v.values,
            xerr=binLength,
            yerr=v.uncertainties,
            fmt="d",
            label=v.label,
            capsize=7,
            capthick=3,
        )
    # Cosmetics of plot
    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    ax.text(0.02, 0.98, text, transform=ax.transAxes, verticalalignment="top")
    plt.tight_layout()
    positionLegend(fig, ax)
    plt.savefig(outName)
    plt.close("all")


def errorBarPlotRatio(
    binCenters: np.ndarray,
    binLength: np.ndarray,
    reference: ValuesAndUncertainties,
    vals: list,
    outName: str,
    xlabel: str = "",
    ylabel: str = "",
    ylabelBot: str = "",
    text: str = "",
    ylim: list = None,
):
    fig, ax = plt.subplots(2, gridspec_kw={"height_ratios": [2, 1]}, sharex=True)

    eps = 1e-10
    # Plot the reference errorbars first
    ax[0].errorbar(
        binCenters,
        reference.values,
        xerr=binLength,
        yerr=reference.uncertainties,
        fmt="d",
        # color="black",
        label=reference.label,
        capsize=7,
        capthick=3,
    )
    ax[1]._get_lines.get_next_color()

    # Plot the remaining errorbars
    for v in vals:
        ax[0].errorbar(
            binCenters,
            v.values,
            xerr=binLength,
            yerr=v.uncertainties,
            fmt="d",
            label=v.label,
            capsize=7,
            capthick=3,
        )

        # Calculate the ratio with respect to the reference and plot the ratio
        ratio = np.divide(v.values, (reference.values + eps))
        error = ratio * np.sqrt(
            (reference.uncertainties / reference.values) ** 2
            + (v.uncertainties / v.values) ** 2
        )

        ax[1].errorbar(
            binCenters,
            ratio,
            xerr=binLength,
            yerr=error,
            fmt="d",
            capsize=7,
            capthick=3,
        )

    ax[1].plot(
        [binCenters[0] - binLength[0], binCenters[-1] + binLength[-1]],
        [1, 1],
        color="grey",
        alpha=0.7,
    )
    # Cosmetics of plot
    ax[0].set_ylabel(ylabel)
    ax[0].text(0.02, 0.98, text, transform=ax[0].transAxes, verticalalignment="top")

    ax[1].set_ylabel(ylabelBot)
    ax[1].set_xlabel(xlabel)
    if ylim is not None:
        ax[1].set_ylim(ylim)

    plt.tight_layout()
    plt.subplots_adjust(hspace=0)
    positionLegend(fig, ax[0])
    fig.savefig(outName)
    plt.close("all")


def convertBins(bins: np.ndarray):
    """
    Convert an array with bin edges into array with [lower edge, upper edge].

    Args
    ----
        bins: *array of floats*
            Numpy array with bin edges.

    Returns
    -------
        newBins: *array of floats*
            Array with one entry for each bin with [lower edge, upper edge]

    """
    newBins = []
    for i in range(len(bins) - 1):
        newBins.append([bins[i], bins[i + 1]])

    return newBins


def getBinCenter(binEdges: np.ndarray, log: bool = False):
    """
    Get bin centers for given bin edges.

    Args
    ----
        binEdges: *array of floats*
            Edges of the bins for which the centers should be calculated
        log: *bool*
            Wheter the binning is on a log scale or not. (Not sure how to do
            this or if this is needed at all)

    Returns
    -------
        *array of floats*
            Array with the center of each bin.

    """
    if log:
        return binEdges[:-1], binEdges[:-1]  # TODO is that needed ?
    else:
        return (
            (binEdges[:-1] + binEdges[1:]) / 2.0,
            (binEdges[1:] - binEdges[:-1]) / 2.0,
        )


def plotBinnedROCCurve(
    config: dict,
    logDir: str,
    truth: np.ndarray,
    plotData: dict,
    preds: list,
    etBins: np.ndarray,
    etaBins: np.ndarray,
    maskClass: np.ndarray = None,
    bkgClass: str = None,
    weights: np.ndarray = None,
    text: str = None,
):
    """
    Plot ROC curves for one model in bins of et and eta using the test set.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed variables: truth, LHVeryLoose, LHLoose, LHLooseBL, LHMedium,
            LHTight, et, eta
        preds: *dict* or *array*
            See in top doc string.
            Both single and multiple model plotting possible.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        maskDict: *dict*
            Dictionary holding different masks such as a mask to only plot the
            ROC curve vs one background.
        maskKey: *str*
            Key for the mask which is to be used out of maskDict.
        weights: *array*
            Weights to applied during the plotting. Pass None if no weights
            should be applied.

    """
    # Iterate over the single et-/etaBins
    for i, (et_low, et_up) in enumerate(convertBins(etBins)):
        for j, (eta_low, eta_up) in enumerate(convertBins(etaBins)):
            # mask to select only electron candidates in this et/etaBin
            if maskClass is not None:
                mask = (
                    (plotData["et"] > et_low)
                    & (plotData["et"] <= et_up)
                    & (np.abs(plotData["eta"]) > eta_low)
                    & (np.abs(plotData["eta"]) <= eta_up)
                    & ((maskClass == 1) | (truth == 1))
                )
            else:
                mask = (
                    (plotData["et"] > et_low)
                    & (plotData["et"] <= et_up)
                    & (np.abs(plotData["eta"]) > eta_low)
                    & (np.abs(plotData["eta"]) <= eta_up)
                )

            if (np.sum([(truth == 1) & (mask == 1)]) <= 1) or (
                np.sum([(truth == 0) & (mask == 1)]) <= 1
            ):
                continue
            if weights is None:
                weights = np.ones(mask.shape[0])
            fig, ax = plt.subplots()

            sigEff = []
            bkgEff = []
            # Calculate signal and bkg effs for each of the LH working points
            for point in ["LHLoose", "LHMedium", "LHTight"]:
                sigEff.append(
                    np.sum(weights[(truth == 1) & (mask == 1) & (plotData[point] == 1)])
                    / np.sum(weights[(truth == 1) & (mask == 1)])
                )
                bkgEff.append(
                    np.sum(weights[(truth == 0) & (mask == 1) & (plotData[point] == 1)])
                    / np.sum(weights[(truth == 0) & (mask == 1)])
                )

            # Plot LH points as black crosses
            sigEff = np.array(sigEff)
            bkgEff = np.array(bkgEff)
            minEff = np.min(sigEff) - 0.1 * (1 - np.min(sigEff))

            # Plot roc curves for given predictions using sklearns roc_curve
            for i in range(len(preds)):
                fpr, tpr, _ = roc_curve(
                    truth[mask], preds[i].preds[mask], sample_weight=weights[mask]
                )
                ax.plot(
                    tpr[tpr > minEff], 1.0 / fpr[tpr > minEff], label=preds[i].label
                )

            ax.plot(
                sigEff[sigEff > minEff],
                1.0 / bkgEff[sigEff > minEff],
                "X",
                color="black",
                label="LH WP",
            )

            # Cosmetics of plot
            ax.set_xlim([minEff, 1])
            ax.set_xlabel("Signal Efficiency")
            if bkgClass is not None:
                ax.set_ylabel(f"{bkgClass} Rejection")
            else:
                ax.set_ylabel(f"Combined Rejection")

            pltText = (
                rf"${et_low} < E_T\ [\mathrm{{GeV}}] \leq $"
                + rf"${et_up}$"
                + "\n"
                + rf"${eta_low} < |\eta| \leq {eta_up}$"
            )
            if text is not None:
                pltText = pltText + "\n" + text
            if bkgClass is not None:
                pltText = pltText + "\n" + f"Background only {bkgClass}"

            ax.text(
                0.02, 0.02, pltText, transform=ax.transAxes, verticalalignment="bottom"
            )

            plt.legend(loc="upper right")
            plt.tight_layout()

            plt.savefig(
                logDir
                + f"/binnedROCCurve{bkgClass}"
                + f"_et{et_low}_{et_up}_eta{eta_low}_{eta_up}.pdf"
                if bkgClass
                else logDir
                + f"/binnedROCCurve_et"
                + f"{et_low}_{et_up}_eta{eta_low}_{eta_up}.pdf"
            )
            plt.close("all")


def plotBinnedROCCurveRatio(
    config,
    logDir,
    truth,
    plotData,
    preds,
    etBins,
    etaBins,
    maskClass=None,
    bkgClass=None,
    weights=None,
    text=None,
):
    """
    Plot ROC curves for one model in bins of et and eta using the test set.

    ...
    """
    # Iterate over the single et-/etaBins
    for i, (et_low, et_up) in enumerate(convertBins(etBins)):
        for j, (eta_low, eta_up) in enumerate(convertBins(etaBins)):
            # mask to select only electron candidates in this et/etaBin
            if maskClass is not None:
                mask = (
                    (plotData["et"] > et_low)
                    & (plotData["et"] <= et_up)
                    & (np.abs(plotData["eta"]) > eta_low)
                    & (np.abs(plotData["eta"]) <= eta_up)
                    & ((maskClass == 1) | (truth == 1))
                )
            else:
                mask = (
                    (plotData["et"] > et_low)
                    & (plotData["et"] <= et_up)
                    & (np.abs(plotData["eta"]) > eta_low)
                    & (np.abs(plotData["eta"]) <= eta_up)
                )

            if (np.sum([(truth == 1) & (mask == 1)]) <= 1) or (
                np.sum([(truth == 0) & (mask == 1)]) <= 1
            ):
                continue
            if weights is None:
                weights = np.ones(mask.shape[0])
            fig, ax = plt.subplots(
                2, gridspec_kw={"height_ratios": [2, 1]}, sharex=True
            )

            sigEff = []
            bkgEff = []
            # Calculate signal and bkg effs for each of the LH working points
            for point in ["LHLoose", "LHMedium", "LHTight"]:
                sigEff.append(
                    np.sum(weights[(truth == 1) & (mask == 1) & (plotData[point] == 1)])
                    / np.sum(weights[(truth == 1) & (mask == 1)])
                )
                bkgEff.append(
                    np.sum(weights[(truth == 0) & (mask == 1) & (plotData[point] == 1)])
                    / np.sum(weights[(truth == 0) & (mask == 1)])
                )

            # Plot LH points as black crosses
            sigEff = np.array(sigEff)
            bkgEff = np.array(bkgEff)
            minEff = np.min(sigEff) - 0.1 * (1 - np.min(sigEff))

            interpolateFunctions = {}
            # Plot roc curves for given predictions using sklearns roc_curve
            for i in range(len(preds)):
                fpr, tpr, _ = roc_curve(
                    truth[mask], preds[i].preds[mask], sample_weight=weights[mask]
                )
                dx = np.concatenate((np.ones(1), np.diff(tpr)))
                nonzeroPlot = (tpr > minEff) & (dx > 0) & (fpr > 0)
                nonzeroInter = (dx > 0) & (fpr > 0)
                ax[0].plot(
                    tpr[nonzeroPlot], 1.0 / fpr[nonzeroPlot], label=preds[i].label
                )
                interpolateFunctions[preds[i].label] = pchip(
                    tpr[nonzeroInter == 1], 1 / fpr[nonzeroInter == 1]
                )
                del tpr, fpr

            for i in range(len(preds)):
                fpr, tpr, _ = roc_curve(
                    truth[mask], preds[i].preds[mask], sample_weight=weights[mask]
                )
                dx = np.concatenate((np.ones(1), np.diff(tpr)))
                nonzero = (tpr > minEff) & (dx > 0) & (fpr > 0) & (tpr < 0.98)
                ratio = interpolateFunctions[preds[i].label](
                    tpr[nonzero]
                ) / interpolateFunctions[preds[0].label](tpr[nonzero])
                ax[1].plot(tpr[nonzero], ratio)

            # Cosmetics of plot
            ax[0].set_xlim([minEff, 0.98])
            ax[1].set_xlabel("Signal Efficiency")
            # Cosmetics of plot
            if bkgClass is not None:
                ax[0].set_ylabel(f"{bkgClass} Rejection")
            else:
                ax[0].set_ylabel(f"Combined Rejection")
            ax[1].set_ylabel(f"Ratio to {preds[0].label}")

            # TODO need to adjust position at some point
            pltText = (
                rf"${et_low} < E_T\ [\mathrm{{GeV}}] \leq $"
                + rf"${et_up}$"
                + "\n"
                + rf"${eta_low} < |\eta| \leq {eta_up}$"
            )
            if text is not None:
                pltText = pltText + "\n" + text
            if bkgClass is not None:
                pltText = pltText + "\n" + f"Background only {maskKey}"

            ax[0].text(
                0.02,
                0.02,
                pltText,
                transform=ax[0].transAxes,
                verticalalignment="bottom",
            )

            # positionLegend(fig, ax[0])
            ax[0].legend(loc="upper right")
            plt.tight_layout()
            plt.subplots_adjust(hspace=0)

            plt.savefig(
                logDir
                + f"/ratiobinnedROCCurve{maskKey}"
                + f"_et{et_low}_{et_up}_eta{eta_low}_{eta_up}.pdf"
                if bkgClass
                else logDir
                + f"/ratiobinnedROCCurve_et"
                + f"{et_low}_{et_up}_eta{eta_low}_{eta_up}.pdf"
            )
            plt.close("all")


def calculateCutValues(
    config: dict,
    truth: np.ndarray,
    plotData: dict,
    preds: np.ndarray,
    etBins: np.ndarray,
    etaBins: np.ndarray,
    workingPoint: str,
):
    """
    Calculate cut values on test set such that sig eff matches LH workingPoint.

    Args
    ----
        plotData: *dict*
            See in top doc string.
            Variables needed: eta, et, pileupWeight, truth, variable given in
            workingPoint.
        preds: *array*
            See in top doc string.
            Only single model at a time possible.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        workingPoint: *str*
            Working point to which the signal efficiency should be matched.

    Returns
    -------
        transCutVals: *array*
            Array containing cut values on the logit transformed NN output
            which match the signal efficiency of the desired working point.

    """
    cutValArr = np.ones((len(etBins) - 1, len(etaBins) - 1))

    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            mask = (
                (np.abs(plotData["eta"]) > etaBins[j])
                & (np.abs(plotData["eta"]) <= etaBins[j + 1])
                & (np.abs(plotData["et"]) > etBins[i])
                & (np.abs(plotData["et"]) <= etBins[i + 1])
                & (truth == 1)
            )

            weights = np.ones(np.sum(mask))  # plotData["p_pileupWeight"][mask]

            weightMask = plotData[workingPoint][mask == 1] == 1
            looseSigEff = np.sum(weights[weightMask == 1]) / np.sum(weights)

            predictions = preds[mask == 1]
            indices = np.argsort(-predictions)

            weights = weights / np.sum(weights)
            cumsum = np.cumsum(weights[indices])
            cutIndex = 0
            for k in range(len(cumsum)):
                if cumsum[k] <= looseSigEff:
                    cutIndex = k

            cutVal = predictions[indices][cutIndex]
            cutValArr[i][j] = cutVal

    return cutValArr


def makePredsToBools(
    config: dict,
    truth: np.ndarray,
    plotData: dict,
    preds: np.ndarray,
    etBins: np.ndarray,
    etaBins: np.ndarray,
    workingPoint: str = "LHLoose",
):
    """
    Load cut values and turn predicitions into bools depending on cut value.

    Args
    ----
        plotData: *dict*
            See in top doc string.
            Needed variables: eta, et
        preds: *array*
            See in top doc string.
            Only single model at the moment.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        workingPoint: *str*
            Cut values which give the same signal efficiency as this working
            point will be used.

    Returns
    -------
        boolPreds: *array*
            Array of bools whether the cut was passed or not.

    """
    etBins = np.array(
        [15, 20, 25, 30, 35, 40, 45, 100]
    )  # 4, 7, 10, 15, 20, 25, 30, 35, 40, 45, 60, 80, 150, 250, 5000
    etaBins = np.array([0.0, 0.1, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37, 2.47])

    transCutVals = calculateCutValues(
        config, truth, plotData, preds, etBins, etaBins, workingPoint
    )

    # Make predictions not equal 0 or 1 to avoid infinities (1/0, log(0))
    boolPreds = np.zeros(preds.shape)

    # Loop through the et- and etaBins since cut value depends on these
    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            mask = (
                (np.abs(plotData["eta"]) > etaBins[j])
                & (np.abs(plotData["eta"]) <= etaBins[j + 1])
                & (np.abs(plotData["et"]) > etBins[i])
                & (np.abs(plotData["et"]) <= etBins[i + 1])
            )

            boolPreds[(mask == 1) & (preds >= transCutVals[i][j])] = 1

    return boolPreds


def getBkgRejBins(
    config: dict,
    truth: np.ndarray,
    plotData: dict,
    boolPreds: np.ndarray,
    etBins: np.ndarray,
    etaBins: np.ndarray,
    label: str,
):
    """
    Calcualte bkg rejection based on predictions given as bools.

    Args
    ----
        plotData: *dict*
            See in top doc string.
            Needed variables: et, eta, pileupWeight
        boolPreds: *array*
            Predictions given as bools. This can be either the predictions of
            an NN transformed into bools or the LH decisions.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.

    Returns
    -------
        bkgRej: *array*
            Backround rejection in each of the et/etaBins
        bkgRejErr: *array*
            Uncertainty of the bkg rejection in each of the et/etaBins

    """
    # Create the arrays to be filled
    bkgRej = np.zeros((len(etBins) - 1, len(etaBins) - 1))
    bkgRejErr = np.zeros((len(etBins) - 1, len(etaBins) - 1))

    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            mask = (
                (np.abs(plotData["eta"]) > etaBins[j])
                & (np.abs(plotData["eta"]) <= etaBins[j + 1])
                & (np.abs(plotData["et"]) > etBins[i])
                & (np.abs(plotData["et"]) <= etBins[i + 1])
                & (truth == 0)
            )

            # Get weights of bkg events in this bin
            weights = np.ones(np.sum(mask))  # plotData["p_pileupWeight"][mask]

            # Get total bkg weights in the bin and the selected bkg weights
            nBkgTot = np.sum(weights)
            nBkgSel = np.sum(weights[boolPreds[mask] == 1])

            # Calculate the rejection and the corresponding Uncertainty
            eff = nBkgSel / nBkgTot
            bkgRej[i][j] = 1 / eff
            bkgRejErr[i][j] = np.sqrt(eff * (1 - eff) / nBkgTot) / eff ** 2

    return ValuesAndUncertainties(bkgRej, bkgRejErr, label)


def plotRejectionPlots(
    config: dict,
    logDir: str,
    truth: np.ndarray,
    plotData: dict,
    preds: np.ndarray,
    etBins: np.ndarray,
    etaBins: np.ndarray,
    mask: np.ndarray = None,
    workingPoint: str = "LHLoose",
):
    """
    Plot rejection of model in bins of et or eta.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed variables: et, eta, truth, pileupWeight,
            variable given by workingPoint
        preds: *array*
            See in top doc string.
            Single and multiple Models are plttable
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        workingPoint: *str*
            Which working point is used to load the cut values and which
            rejection is plotted as reference.

    """
    truth = truth[mask == 1]
    for key in plotData:
        plotData[key] = plotData[key][mask == 1]
    for p in preds:
        p.preds = p.preds[mask == 1]
    lh2Dvals = getBkgRejBins(
        config, truth, plotData, plotData[workingPoint], etBins, etaBins, workingPoint
    )

    if "Loose" in workingPoint:
        maskCuts = plotData["ambiguityBit"] >= 5
    elif "Medium" in workingPoint:
        maskCuts = (plotData["ambiguityBit"] >= 5) | (plotData["nBlayerCut"] == 0)
    elif "Tight" in workingPoint:
        maskCuts = (plotData["ambiguityBit"] >= 2) | (plotData["nBlayerCut"] == 0)
    maskCuts = np.ones(plotData["eta"].shape)
    vals2D = []
    print(len(preds))
    for i in range(len(preds)):
        p = preds[i].getPredictionsWithCut(maskCuts)
        boolPreds = makePredsToBools(
            config, truth, plotData, p, etBins, etaBins, workingPoint=workingPoint
        )
        vals2D.append(
            getBkgRejBins(
                config, truth, plotData, boolPreds, etBins, etaBins, preds[i].label
            )
        )

    etBinsCenter = getBinCenter(etBins)
    etaBinsCenter = getBinCenter(etaBins)
    etBinsCenter[0][-1] = etBinsCenter[0][-2] + etBinsCenter[1][-2] + 20
    etBinsCenter[1][-1] = 20

    # for i in range(len(etBins) - 1):
    #     lhEta = lh2Dvals.getAxis(index=i, axis=0)
    #     etaVals = []
    #     for v in vals2D:
    #         etaVals.append(v.getAxis(index=i, axis=0))
    #
    #     errorBarPlotRatio(
    #         etaBinsCenter[0],
    #         etaBinsCenter[1],
    #         lhEta,
    #         etaVals,
    #         logDir + f"/rejection{workingPoint}_et" + f"_{etBins[i]}_{etBins[i+1]}.pdf",
    #         xlabel=r"$|\eta|$",
    #         ylabel="1 / Background Efficiency",
    #         ylabelBot="Ratio to LH",
    #         text=f"Signal efficiencies \ncorresponding to "
    #         + f"{workingPoint}\n"
    #         + rf"${etBins[i]} < E_T\ [\mathrm{{GeV}}]"
    #         + rf"\leq {etBins[i+1]}$",
    #         ylim=[0.5, 4.5],
    #     )

    for i in range(len(etaBins) - 1):
        # lhEt = lh2Dvals.getAxis(index=i, axis=1)
        etVals = []
        for v in vals2D:
            etVals.append(v.getAxis(index=i, axis=1))

        errorBarPlotRatio(
            etBinsCenter[0],
            etBinsCenter[1],
            etVals[0],
            [],  # [etVals[1], etVals[2], etVals[3]],
            logDir
            + f"/rejection{workingPoint}_"
            + f"eta_{etaBins[i]}_{etaBins[i+1]}.pdf",
            xlabel=r"$E_T$",
            ylabel="Bkg Rejection",
            ylabelBot=f"Ratio to {etVals[0].label}",
            text=f"Signal efficiencies \ncorresponding to "
            + f"{workingPoint}\n"
            + rf"${etaBins[i]} < |\eta| \leq "
            + rf"{etaBins[i+1]}$",
            # + "\nLast bin "
            # + f"contains all events\nuntil {etBins[-1]} "
            # + "GeV",
            ylim=[0.6, 1.4],
        )
