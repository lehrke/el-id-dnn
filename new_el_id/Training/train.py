import argparse
from new_el_id.CommonTools import mkdir, CustomParser
from new_el_id.Training import DataHandler, ModelContainer, TrainConfiguration


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = CustomParser(description="???")

    parser.add_argument(
        "-l",
        "--logDir",
        action="store",
        type=str,
        help="Directory where all outputs are stored.",
    )
    parser.add_argument(
        "-t",
        "--task",
        action="store",
        type=str,
        choices=["train", "predict"],
        help="Train a new model or use an existing one to " + "predict on a dataset.",
    )
    parser.add_argument(
        "-f",
        "--testFile",
        action="store",
        type=str,
        help="Calculate outputs of network using this file.",
    )
    parser.add_argument(
        "-p",
        "--predsName",
        action="store",
        type=str,
        help="Predictions will be saved under this name + "
        + ".npy in the logDir. The file ending .npy is "
        + "automatically added.",
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = getParser()
    configLoader = TrainConfiguration(args.configFile)
    configLoader.replaceWithCommandLineArguments(args)
    config = configLoader.config

    mkdir(config["logDir"])
    dataHandler = DataHandler(config)

    config = configLoader.UpdateTrainStepsPerEpoch(dataHandler.getShapeTrainData())
    configLoader.dump(config["logDir"] + "/config.yaml")

    modelCont = ModelContainer(config)

    if config["task"] == "train":
        config = configLoader.UpdateTrainStepsPerEpoch(dataHandler.getShapeTrainData())
        configLoader.dump(config["logDir"] + "/config.yaml")

        modelCont = ModelContainer(config)
        modelCont.buildAndCompile()
        modelCont.dumpArchitecture()

        X, Y, weights = dataHandler.loadAndPreprocessData("train", fit=True)
        Xval, Yval, weightsVal = dataHandler.loadAndPreprocessData("val", fit=False)

        modelCont.trainModel((X, Y, weights), (Xval, Yval, weightsVal))

        modelCont.saveWeights()
        del X, Y, weights, Xval, Yval, weightsVal
    elif config["task"] == "predict":

        modelCont.createFromArchitecture()

    else:
        raise ValueError("task has to be either 'train' or 'predict'")

    modelCont.loadWeights()

    X, Y, _ = dataHandler.loadAndPreprocessData("test", fit=False)

    modelCont.predictAndSave(X)

    if config["task"] == "train":
        modelCont.plotControlPlots(Y)
