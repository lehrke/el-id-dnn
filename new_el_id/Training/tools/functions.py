import numpy as np
import joblib
import h5py
import pandas as pd
import tensorflow as tf

tf.config.optimizer.set_jit(True)

from new_el_id.CommonTools import (
    Configuration,
    mkdir,
    weightedQuantileTransformer,
    rcParams,
    positionLegend,
    plotHistogram,
)
from new_el_id.Training.tools.callbacks import CyclicLR
from tensorflow.keras.regularizers import l2
import tensorflow.keras.callbacks as cb
from sklearn.preprocessing import QuantileTransformer

# Make sure that plotting works while running on a cluster
import matplotlib

matplotlib.use("Agg")

import matplotlib.pyplot as plt

plt.rcParams.update(rcParams)


class TrainConfiguration(Configuration):
    def __init__(self, configFile: str):
        super(TrainConfiguration, self).__init__(configFile, training=True)

    def UpdateTrainStepsPerEpoch(self, nTrainPoints) -> dict:
        self.config["trainStepsPerEpoch"] = (
            int(nTrainPoints / self.config["batchSize"]) + 1
        )
        if self.config["callbacks"]["lrSchedule"]:
            if self.config["callbacks"]["lrSchedule"]["config"]:
                self.config["callbacks"]["lrSchedule"]["config"][
                    "trainStepsPerEpoch"
                ] = self.config["trainStepsPerEpoch"]

        return self.config


class DataHandler:
    def __init__(self, config: dict):
        self.config = config
        self.variableNames = self.config["VariableNames"]
        self.logDir = self.config["logDir"]
        self.inputFileTrain = self.config["inputs"]["train"]
        self.inputFileVal = self.config["inputs"]["val"]
        self.inputFileTest = self.config["inputs"]["test"]
        self.inputVars = self.config["inputVars"]
        self.multiClass = self.config["multiClass"]
        self.weightName = self.config["weightName"]
        self.additionalPreprocessing = self.config["additionalPreprocessing"]

    def getShapeTrainData(self) -> int:
        shapeX = 0
        with h5py.File(self.inputFileTrain, "r") as hf:
            shapeX = hf[self.variableNames[self.inputVars[0]]].shape[0]

        return shapeX

    def loadAndPreprocessData(self, sett: str, fit: bool):
        data = {}
        if sett == "train":
            inputFile = self.inputFileTrain
        elif sett == "val":
            inputFile = self.inputFileVal
        elif sett == "test":
            inputFile = self.inputFileTest
        with h5py.File(inputFile, "r") as hf:
            for var in self.inputVars:
                data[self.variableNames[var]] = hf[self.variableNames[var]][:]

            if not self.multiClass:
                iff = hf[self.variableNames["iff"]][:]
                mask = iff != 3

                y = (iff == 2)[mask == 1]
                for key in data:
                    data[key] = data[key][mask == 1]
            else:
                iff = hf[self.variableNames["iff"]][:]
                tt = hf[self.variableNames["truthType"]][:]

                y = np.zeros(iff.shape)
                y[iff == 3] = 1
                y[iff == 5] = 2
                y[(iff == 8) | (iff == 9)] = 3
                y[(iff == 10) & (tt < 17)] = 4
                y[(iff == 10) & (tt == 17)] = 5
                y = tf.keras.utils.to_categorical(y)

            if self.weightName is not None:
                weights = hf[self.weightName][:]
                if not self.multiClass:
                    weights = weights[mask == 1]
            else:
                weights = np.ones(y.shape[0])

        if self.additionalPreprocessing["abseta"]:
            data[self.variableNames["eta"]] = np.abs(data[self.variableNames["eta"]])
        if self.additionalPreprocessing["maskf3"]:
            data[self.variableNames["f3"]][
                np.abs(data[self.variableNames["eta"]]) > 2.01
            ] = 0.05
        if self.additionalPreprocessing["maskTRT"]:
            data[self.variableNames["TRTPID"]][
                (np.abs(data[self.variableNames["eta"]]) > 2.01)
                & (np.abs(data[self.variableNames["TRTPID"]]) < 1e-6)
            ] = 0.15

        if self.variableNames["TRTPID"] in data:
            data[self.variableNames["TRTPID"]][
                data[self.variableNames["TRTPID"]] == np.inf
            ] = 2
        if self.variableNames["TRTPIDRNN"] in data:
            data[self.variableNames["TRTPIDRNN"]][
                data[self.variableNames["TRTPIDRNN"]] == np.inf
            ] = 2

        X = np.array([data[self.variableNames[var]] for var in self.inputVars]).T

        if fit:
            self.qt = weightedQuantileTransformer(X, np.linspace(0, 1, 1000), weights)
            joblib.dump(self.qt, self.logDir + "/quantile.transformer")

        if not hasattr(self, "qt"):
            self.qt = joblib.load(self.logDir + "/quantile.transformer")

        X = self.qt.transform(X)

        return X, y, weights


class ModelContainer:
    def __init__(self, config: dict):
        super(ModelContainer, self).__init__()
        self.modelParameters = config["modelParameters"]
        self.nInputs = len(config["inputVars"])
        self.multiClass = config["multiClass"]
        self.optimizer = config["optimizer"]
        self.loss = config["loss"]
        self.metrics = config["metrics"]
        self.weightedMetrics = config["weightedMetrics"]
        self.logDir = config["logDir"]
        self.callbacks = config["callbacks"]
        self.batchSize = config["batchSize"]
        self.epochs = config["epochs"]
        self.verbose = config["verbose"]
        self.predsName = config["predsName"]
        self.trainStepsPerEpoch = config["trainStepsPerEpoch"]

    def buildAndCompile(self) -> None:
        inputs = tf.keras.layers.Input(shape=(self.nInputs,), name="ScalarInputs")

        for i, n in enumerate(self.modelParameters["architecture"]):
            if i == 0:
                tns = tf.keras.layers.Dense(
                    n, kernel_regularizer=l2(self.modelParameters["regularization"])
                )(inputs)
            else:
                tns = tf.keras.layers.Dense(
                    n, kernel_regularizer=l2(self.modelParameters["regularization"])
                )(tns)

            tns = _getActivation(self.modelParameters["activation"])(tns)

            if self.modelParameters["doBatchNorm"]:
                tns = tf.keras.layers.BatchNormalization()(tns)

            if self.modelParameters["dropout"] > 0:
                tns = tf.keras.layers.Dropout(self.modelParameters["dropout"])(tns)

        if not self.multiClass:
            outputs = tf.keras.layers.Dense(
                1,
                activation="sigmoid",
                kernel_regularizer=l2(self.modelParameters["regularization"]),
            )(tns)
        else:
            outputs = tf.keras.layers.Dense(
                6,
                activation="softmax",
                kernel_regularizer=l2(self.modelParameters["regularization"]),
            )(tns)

        self.model = tf.keras.Model(inputs=inputs, outputs=outputs)

        print(self.model.summary())

        self.model.compile(
            optimizer=_getOptimizer(self.optimizer),
            loss=self.loss,
            metrics=self.metrics,
            weighted_metrics=self.weightedMetrics,
        )

    def dumpArchitecture(self) -> None:
        architecture = self.model.to_json()
        with open(self.logDir + "/architecture.json", "w") as archFile:
            archFile.write(architecture)

    def createFromArchitecture(self) -> None:
        with open(self.logDir + "/architecture.json", "r") as jsonFile:
            arch = jsonFile.read()

        self.model = tf.keras.models.model_from_json(arch)

    def trainModel(self, trainData: np.ndarray, valData: np.ndarray) -> None:
        if self.weightedMetrics:
            self.cbMetric = "val_weighted_" + self.weightedMetrics[0]
            self.plotMetric = "weighted_" + self.weightedMetrics[0]
        else:
            self.cbMetric = "val_" + self.metrics[0]
            self.plotMetric = self.metrics[0]

        callbacks = _getCallbacks(
            logDir=self.logDir,
            metric=self.cbMetric,
            earlyStopping=self.callbacks["earlyStopping"],
            lrSchedule=self.callbacks["lrSchedule"],
        )

        self.model.fit(
            trainData[0],
            trainData[1],
            batch_size=self.batchSize,
            epochs=self.epochs,
            verbose=self.verbose,
            callbacks=callbacks,
            sample_weight=pd.Series(trainData[2]),
            validation_data=(valData[0], valData[1], pd.Series(valData[2])),
        )

    def saveWeights(self) -> None:
        self.model.save_weights(self.logDir + "/weightsForLwtnn.h5")

    def loadWeights(self) -> None:
        self.model.load_weights(self.logDir + "/bestModel.h5")

    def predictAndSave(self, testData: np.ndarray) -> None:
        self.preds = self.model.predict(testData, batch_size=self.batchSize)

        if self.multiClass:
            self.preds = self.preds.reshape(-1, 6)
        else:
            self.preds = self.preds.flatten()

        np.save(self.logDir + "/" + self.predsName + ".npy", self.preds)

    def plotControlPlots(self, y: np.ndarray) -> None:
        mkdir(self.logDir + "/ControlPlots/")

        # Loss vs Epoch
        plotTrainValLoss(self.logDir, self.plotMetric)

        clas = ["Signal", "CF", "PC", "HF", "LFEg", "LFH"]
        bins = np.linspace(0, 1, 101)
        if self.multiClass:
            for i in range(6):
                plotHistogram(
                    data=self.preds[:, i],
                    mask=[y[:, j] == 1 for j in range(6)],
                    labels=clas,
                    weights=None,
                    bins=bins,
                    outputName=self.logDir + f"/ControlPlots/Scores{clas[i]}.pdf",
                    ylog=True,
                    xlabel=f"{clas[i]} output node",
                )

        else:
            plotHistogram(
                data=self.preds,
                mask=[y == 1, y == 0],
                labels=["Signal", "Bkg"],
                weights=None,
                bins=bins,
                outputName=self.logDir + f"/ControlPlots/Scores.pdf",
                ylog=True,
                xlabel=f"DNN output",
            )


def getEpochLoss(logFile: str, metric: str):
    """
    Read out the loss or a metric per epoch for a tensorboard file.

    Args
    ----
        logFile: *str*
            Tensorboard file which has the loss and the metrics saved.
        metric: *str*
            Metric to read out.

    Returns
    -------
        loss: *list*
            List of the loss/metric values for each epoch
        lossEpoch: *list*
            List of the number of epochs.

    """
    import tensorboard.backend.event_processing.event_accumulator as evt
    import glob
    import os

    logs = glob.glob(logFile)
    fileIn = max(logs, key=os.path.getctime)
    ea = evt.EventAccumulator(
        fileIn,
        size_guidance={
            evt.COMPRESSED_HISTOGRAMS: 1,
            evt.IMAGES: 1,
            evt.AUDIO: 1,
            evt.SCALARS: 0,
            evt.HISTOGRAMS: 1,
        },
    )
    ea.Reload()

    loss = []
    lossEpoch = np.arange(len(ea.Scalars("epoch_" + metric))) + 1
    for epoch in ea.Scalars("epoch_" + metric):
        loss.append(epoch.value)

    return loss, lossEpoch


def plotTrainValLoss(logDir: str, metric: str) -> None:
    """
    Plot the training and validation loss during the training.

    Args
    ----
        logDir: *str*
            See in top doc string.
        metric: *str*
            Metric which should be plotted. This has to be the exact same
            spelling as it is given to the model. For instance
            "binary_crossentropy" for binary crossentropy.

    """
    trainLoss, trainLossEpoch = getEpochLoss(
        logDir + "/train/events*v2", metric.replace("val_", "")
    )
    valLoss, valLossEpoch = getEpochLoss(
        logDir + "/validation/events*v2", metric.replace("val_", "")
    )

    metrics = {
        "loss": "Loss",
        "binary_crossentropy": "Binary CE",
        "weighted_binary_crossentropy": "Binary CE",
        "categorical_crossentropy": "Categorical CE",
        "weighted_categorical_crossentropy": "Categorical CE",
    }

    plotMetric = metrics[metric]
    fig, ax = plt.subplots(figsize=(19.20, 12.00))
    ax.plot(trainLossEpoch, trainLoss, label=f"Training {plotMetric}")
    ax.plot(valLossEpoch, valLoss, label=f"Validation {plotMetric}")
    ax.plot(
        valLossEpoch,
        [np.min(valLoss)] * len(valLossEpoch),
        label=f"Minimal Validation {plotMetric} at Epoch " + f"{np.argmin(valLoss)+1}",
        color="black",
        linestyle="--",
        alpha=0.5,
    )
    ax.set_xlabel("Epoch")
    ax.set_ylabel(f"{plotMetric}")
    positionLegend(fig, ax)
    plt.tight_layout()
    fig.savefig(logDir + f"/ControlPlots/LossVsEpoch.pdf")
    plt.close("all")


def _getActivation(activation):  # str or dict
    """
    Return an instance of the desired activation function.

    Args
    ----
        activation : *str* or *dict*
            The name of an activation function.
            One of "relu", "leakyrelu", "elu".
            Or a config dict.

    Returns
    -------
        *Keras layer instance*

    """
    if isinstance(activation, dict):
        act, kwargs = activation["className"], activation["config"]
    else:
        act = activation
        kwargs = {}

    act = activation.lower()
    if act == "leakyrelu":
        return tf.keras.layers.LeakyReLU(**kwargs)
    elif act == "elu":
        return tf.keras.layers.ELU(**kwargs)
    elif act == "relu":
        return tf.keras.layers.ReLU(**kwargs)
    else:
        raise ValueError("Activation function " + activation + " not supported.")


def _getOptimizer(optimizer):  # str or dict
    """
    Return an instance of the desired optimizer.

    Args
    ----
        optimizer : *str* or *dict*
            The name of an optimizer or a config dict.
            One of "adam".

    Returns
    -------
        *TensorFlow optimizer*

    """
    if isinstance(optimizer, dict):
        optimizer, kwargs = optimizer["className"], optimizer["config"]
    else:
        kwargs = {}

    opt = optimizer.lower()
    if opt == "adam":
        return tf.keras.optimizers.Adam(**kwargs)
    else:
        raise ValueError("Optimizer " + optimizer + " not supported.")


def _getCallbacks(
    logDir: str, metric: str, earlyStopping: int, lrSchedule=None
) -> list:  # str or dict
    """
    Register all callbacks.

    Args
    ----
        logDir : *str*
            Directory everything will be stored in.

        metric : *str*
            Metric which will be used for early stopping
            and monitoring the best model.

        earlyStopping : *int*
            Number of epochs for early stopping.
            If earlyStoppingRound <= 0 no early stopping
            is applied.


    Returns
    -------
        *list of TensorFlow callbacks*

    """
    callbacks = []

    tensorboard = cb.TensorBoard(log_dir=logDir)
    callbacks.append(tensorboard)

    modelCheckpoint = cb.ModelCheckpoint(
        filepath=logDir + "/bestModel.h5",
        monitor=metric,
        verbose=0,
        save_best_only=True,
    )
    callbacks.append(modelCheckpoint)

    if earlyStopping > 0:
        earlyStoppingCallback = cb.EarlyStopping(monitor=metric, patience=earlyStopping)
        callbacks.append(earlyStoppingCallback)

    if lrSchedule is not None:
        if lrSchedule["className"] == "cyclicLR":
            lrScheduleCB = CyclicLR(**lrSchedule["config"])
            callbacks.append(lrScheduleCB)
        elif lrSchedule["className"] == "plateau":
            lrScheduleCB = cb.ReduceLROnPlateau(**lrSchedule["config"])
            callbacks.append(lrScheduleCB)

    return callbacks
