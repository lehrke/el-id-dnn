from tensorflow.keras.callbacks import Callback
import tensorflow.keras.backend as K
import numpy as np


class CyclicLR(Callback):
    """This callback implements a cyclical learning rate policy (CLR).

    Adapted from
    https://gitlab.com/ffaye/deepcalo/blob/master/deepcalo/callbacks/cyclic_lr_schedule.py
    The method cycles the learning rate between two boundaries with
    some constant frequency, as detailed in this paper
    (https://arxiv.org/abs/1506.01186).
    The amplitude of the cycle can be scaled on a per-iteration or
    per-cycle basis.
    This class has three built-in policies, as put forth in the paper.
    "triangular":
        A basic triangular cycle w/ no amplitude scaling.
    "triangular2":
        A basic triangular cycle that scales initial amplitude by half each
        cycle.
    "expRange":
        A cycle that scales initial amplitude by gamma**(cycle iterations) at
        each cycle iteration.
    For more detail, please see paper.

    # Example
        ```python
            clr = CyclicLR(baseLr=0.001, maxLr=0.006,
                           stepSize=2000., mode='triangular')
            model.fit(X_train, Y_train, callbacks=[clr])
        ```

    Class also supports custom scaling functions:
        ```python
            clrFn = lambda x: 0.5*(1+np.sin(x*np.pi/2.))
            clr = CyclicLR(baseLr=0.001, maxLr=0.006,
                           stepSize=2000., scaleFn=clrFn,
                           scaleMode='cycle')
            model.fit(X_train, Y_train, callbacks=[clr])
        ```
    """

    def __init__(
        self,
        baseLr: float = 0.001,
        increaseLr: float = 50,
        nEpochs: int = 20,
        trainStepsPerEpoch: int = 10000,
        mode: str = "triangular",
        gamma: float = 1,
        scaleFn=None,
        scaleMode: str = "cycle",
    ):
        """
        Initialize CyclicLR class.

        Args
        ----
            baseLr: *float*
                initial learning rate which is the lower boundary in the cycle.
            maxLr: *float*
                upper boundary in the cycle. Functionally,
                it defines the cycle amplitude (maxLr - baseLr).
                The lr at any cycle is the sum of baseLr
                and some scaling of the amplitude; therefore
                maxLr may not actually be reached depending on
                scaling function.
            stepSize: *int*
                number of training iterations per
                half cycle. Authors suggest setting stepSize
                2-8 x training iterations in epoch.
            mode: *str*
                one of {triangular, triangular2, expRange}.
                Default 'triangular'.
                Values correspond to policies detailed above.
                If scaleFn is not None, this argument is ignored.
            gamma: *float*
                constant in 'expRange' scaling function:
                gamma**(cycle iterations)
            scaleFn: *function*
                Custom scaling policy defined by a single
                argument lambda function, where
                0 <= scaleFn(x) <= 1 for all x >= 0.
                mode paramater is ignored
            scaleMode: *str*
                {'cycle', 'iterations'}.
                Defines whether scaleFn is evaluated on
                cycle number or cycle iterations (training
                iterations since start of cycle). Default is 'cycle'.
        """
        super(CyclicLR, self).__init__()

        self.baseLr = baseLr
        self.maxLr = baseLr * increaseLr
        self.nEpochs = nEpochs
        self.trainStepsPerEpoch = trainStepsPerEpoch
        self.stepSize = self.nEpochs * self.trainStepsPerEpoch
        self.mode = mode
        self.gamma = gamma
        if scaleFn is None:
            if self.mode == "triangular":
                self.scaleFn = lambda x: 1.0
                self.scaleMode = "cycle"
            elif self.mode == "triangular2":
                self.scaleFn = lambda x: 1 / (2.0 ** (x - 1))
                self.scaleMode = "cycle"
            elif self.mode == "expRange":
                self.scaleFn = lambda x: gamma ** (x)
                self.scaleMode = "iterations"
        else:
            self.scaleFn = scaleFn
            self.scaleMode = scaleMode
        self.clrIterations = 0.0
        self.trnIterations = 0.0
        self.history = {}

        self._reset()

    def _reset(self, newBaseLr=None, newMaxLr=None, newStepSize=None):
        """Reset cycle iterations.

        Optional boundary/step size adjustment.
        """
        if newBaseLr is not None:
            self.baseLr = newBaseLr
        if newMaxLr is not None:
            self.maxLr = newMaxLr
        if newStepSize is not None:
            self.stepSize = newStepSize
        self.clrIterations = 0.0

    def clr(self):
        """Calculate updated learning rate."""
        cycle = np.floor(1 + self.clrIterations / (2 * self.stepSize))
        x = np.abs(self.clrIterations / self.stepSize - 2 * cycle + 1)
        if self.scaleMode == "cycle":
            return self.baseLr + (self.maxLr - self.baseLr) * np.maximum(
                0, (1 - x)
            ) * self.scaleFn(cycle)
        else:
            return self.baseLr + (self.maxLr - self.baseLr) * np.maximum(
                0, (1 - x)
            ) * self.scaleFn(self.clrIterations)

    def on_train_begin(self, logs={}):
        """Execute at the start of the training."""
        logs = logs or {}

        if self.clrIterations == 0:
            if hasattr(self.model.optimizer, "lr"):
                K.set_value(self.model.optimizer.lr, self.baseLr)
            else:
                K.set_value(self.model.optimizer.optimizer._lr, self.baseLr)
        else:
            if hasattr(self.model.optimizer, "lr"):
                K.set_value(self.model.optimizer.lr, self.clr())
            else:
                K.set_value(self.model.optimizer.optimizer._lr, self.clr())

    def on_train_batch_end(self, epoch, logs=None):
        """Execute after one batch is processed during the training."""
        logs = logs or {}
        self.trnIterations += 1
        self.clrIterations += 1

        if hasattr(self.model.optimizer, "lr"):
            lr = K.get_value(self.model.optimizer.lr)
        else:
            lr = K.get_value(self.model.optimizer.optimizer._lr)
        self.history.setdefault("lr", []).append(lr)

        self.history.setdefault("iterations", []).append(self.trnIterations)

        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)

        if hasattr(self.model.optimizer, "lr"):
            K.set_value(self.model.optimizer.lr, self.clr())
        else:
            K.set_value(self.model.optimizer.optimizer._lr, self.clr())
