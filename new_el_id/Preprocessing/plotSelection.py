"""Make plots of defined input variables from an h5 file."""
from new_el_id.CommonTools import Configuration, CustomParser
from new_el_id.Preprocessing import PreprocessingPlotting
import argparse


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = CustomParser(description="Options for plotting")

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = getParser()
    configLoader = Configuration(
        args.configFile, args.commonConfigFile, preprocessing=True
    )
    configLoader.replaceWithCommandLineArguments(args)
    config = configLoader.config

    for key in config["split"]:
        inputFile = (
            config["outDirConversion"]
            + f"/VDS_{config['outFileNameConversion']}_{key}.h5"
        )
        plotter = PreprocessingPlotting(
            config, inputFile, key, weights=None, selection=True
        )
        plotter.plotVars()
