"""Transform a ROOT TTree into an .h5 file."""
from new_el_id.CommonTools import Configuration, mkdir, CustomParser
from new_el_id.Preprocessing import NtupleTransformer
import argparse


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = CustomParser(description="Options for converting TTree into h5")

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = getParser()

    configLoader = Configuration(
        args.configFile, args.commonConfigFile, preprocessing=True
    )
    configLoader.replaceWithCommandLineArguments(args)
    config = configLoader.config

    print(config)
    transformer = NtupleTransformer(config)
    transformer.transform()
    transformer.combineOutputsToVDSFile()
