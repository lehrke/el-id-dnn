import numpy as np
import h5py
import glob
import uproot3 as uproot
import numpy as np

# Make sure that plotting works while running on a cluster
import matplotlib

matplotlib.use("Agg")

import matplotlib.pyplot as plt
import copy
from new_el_id.CommonTools import (
    mkdir,
    positionLegend,
    rcParams,
    plotHistogram,
    PlottingBase,
)

plt.rcParams.update(rcParams)


class NtupleTransformer:
    def __init__(self, config):
        self.config = config
        mkdir(self.config["outDirConversion"])
        self.savedFiles = {}
        self.totalSplit = 0
        for key in self.config["split"]:
            self.savedFiles[key] = []
            self.totalSplit += self.config["split"][key]

    def transform(self):
        """
        Load data from ROOT TTree and save it in a .h5 file.
        """
        for process in self.config["inputNtupleDirs"]:
            rootFileNames = glob.glob(self.config["inputNtupleDirs"][process] + "/*")
            mkdir(self.config["outDirConversion"] + f"/{process}")

            outFileName = (
                self.config["outDirConversion"]
                + f"/{process}/"
                + self.config["outFileNameConversion"]
                + f"_{process}"
            )
            for i, fileName in enumerate(rootFileNames):
                tree = uproot.open(fileName)[self.config["treeName"]]

                splitNumber = tree.array(self.config["splitVariable"]) % self.totalSplit
                if splitNumber.shape[0] == 0:
                    continue

                maskSplitting = {}
                allSplits = 0
                for key in self.config["split"]:
                    maskSplitting[key] = (
                        splitNumber < self.config["split"][key] + allSplits
                    ) & (splitNumber >= allSplits)
                    allSplits += self.config["split"][key]

                for j, key in enumerate(tree.keys()):
                    data = tree.array(key)
                    for maskKey in maskSplitting:
                        if i == 0:
                            self.saveToH5(
                                outFileName,
                                data[maskSplitting[maskKey] == 1],
                                key,
                                j,
                                maskKey,
                            )
                        else:
                            self.appendToH5(
                                outFileName,
                                data[maskSplitting[maskKey] == 1],
                                key,
                                maskKey,
                            )

    def saveToH5(self, outFileName, data, key, keyNumber, maskKey):
        """
        Save data to an .h5 file.

        Args
        ----
            outFileName: *str*
                Name of the .h5 file where the data will be saved.
            data: *np.array*
                Data to be saved in the .h5 file
            key: *str*
                Name of the variable which is being saved. The data is saved in a
                dataset with this name.
            keyNumber: *int*
                Integer how often something was already saved into the .h5 file.
                The first time the file has to be created, whereas the other times
                the data needs to be appended.
            maskKey: *dict*
                Name of the subset of the data. Will be saved in the name.

        """
        outName = outFileName + f"_{maskKey}.h5"
        if outName not in self.savedFiles[maskKey]:
            self.savedFiles[maskKey].append(outName)
        with h5py.File(outName, "w" if keyNumber == 0 else "a") as hf:
            key = key.decode("utf-8")
            hf.create_dataset(
                key,
                data=data,
                chunks=True,
                maxshape=(None,),
                compression="lzf",
            )

    def appendToH5(self, outFileName, data, key, maskKey):
        """
        Append data to an .h5 file if the variable already exists in the .h5 file.

        Args
        ----
            outFileName: *str*
                Name of the .h5 file where the data will be saved.
            data: *np.array*
                Data to be saved in the .h5 file
            key: *str*
                Name of the variable which is being saved. The data is saved in a
                dataset with this name.
            maskKey: *dict*
                Name of the subset of the data. Will be saved in the name.

        """
        with h5py.File(outFileName + f"_{maskKey}.h5", "a") as hf:
            key = key.decode("utf-8")
            nNew = data.shape[0]
            hf[key].resize(hf[key].shape[0] + nNew, axis=0)
            hf[key][-nNew:] = data

    def combineOutputsToVDSFile(self):
        for key in self.savedFiles:
            createVDSFile(
                self.savedFiles[key],
                self.config["outDirConversion"]
                + "/VDS_"
                + self.config["outFileNameConversion"]
                + f"_{key}.h5",
            )


def createVDSFile(inputFiles, outputFile):
    # Get the name of all datasets from the first input file
    with h5py.File(inputFiles[0], "r") as hf:
        keys = [key for key in hf.keys()]

    # iterate over train, val, and test set
    shapes = np.ones(len(inputFiles))
    # iterate over the input files to get the shapes of every one of them
    for i, infile in enumerate(inputFiles):
        with h5py.File(infile, "r") as hf:
            shapes[i] = hf[f"{keys[0]}"].shape[0]

    cumsum = np.cumsum(shapes)
    cumsum = np.concatenate((np.array([0]), cumsum))

    # iterate over the single datasets
    for j, var in enumerate(keys):
        # create what will become the virtual dataset
        layout = h5py.VirtualLayout(shape=(int(np.sum(shapes)),))

        # iterate over the input files filling the virtual dataset
        for n, infile in enumerate(inputFiles):
            vsource = h5py.VirtualSource(infile, f"{var}", shape=(int(shapes[n]),))
            layout[int(cumsum[n]) : int(cumsum[n + 1])] = vsource

        # create the dataset in the output file.
        # If first time here create the output file
        if j == 0:
            with h5py.File(outputFile, "w", libver="latest") as f:
                f.create_virtual_dataset(f"{var}", layout)
        else:
            with h5py.File(outputFile, "a", libver="latest") as f:
                f.create_virtual_dataset(f"{var}", layout)


class PreprocessingPlotting(PlottingBase):
    def __init__(
        self,
        config: dict,
        fileName: str,
        sett: str,
        weights: str = None,
        selection: bool = False,
        downsampling: bool = False,
        weighting: bool = False,
        classWeighting: bool = False,
    ):
        super(PreprocessingPlotting, self).__init__(
            config=config, fileName=fileName, weights=weights
        )
        self.sett = sett
        self.classes = self.config["classes"]
        if selection + downsampling + weighting + classWeighting != 1:
            raise ValueError(
                "Exactly one of selection, downsampling, and weighting needs to be set to True"
            )
        if selection:
            self.plotOutputName = self.config["selectionNames"]["outName"]
            self.text = self.config["selectionNames"]["text"]
        if downsampling:
            self.plotOutputName = self.config["downsamplingNames"]["outName"]
            self.text = self.config["downsamplingNames"]["text"]
        if weighting:
            self.plotOutputName = self.config["weightingNames"]["outName"]
            self.text = self.config["weightingNames"]["text"]
        if classWeighting:
            self.plotOutputName = self.config["classWeightingNames"]["outName"]
            self.text = self.config["classWeightingNames"]["text"]

    def plotVars(self) -> None:
        self._loadTruthInfo()
        self._loadWeights()
        mkdir(self.outDir + "/Plots")

        for var in self.config["plotVars"]:
            self._loadVariable(var)
            if self.config["variables"][var]["bins"]["log"]:
                bins = np.logspace(
                    np.log10(self.config["variables"][var]["bins"]["low"]),
                    np.log10(self.config["variables"][var]["bins"]["high"]),
                    self.config["variables"][var]["bins"]["number"],
                )
            else:
                bins = np.linspace(
                    self.config["variables"][var]["bins"]["low"],
                    self.config["variables"][var]["bins"]["high"],
                    self.config["variables"][var]["bins"]["number"],
                )

            plotHistogram(
                self.data[var],
                [self.truth == 1, self.truth == 0],
                ["Signal", "Bkg"],
                self.weights,
                bins,
                self.outDir
                + f"/Plots/{self.plotOutputName}_Binary_{self.config['variables'][var]['fileName']}_{self.sett}.pdf",
                xlog=self.config["variables"][var]["xlog"],
                ylog=self.config["variables"][var]["ylog"],
                xlabel=self.config["variables"][var]["plotName"],
                text=self.text + "\n" + self.config["dataset"],
            )

            plotHistogram(
                self.data[var],
                [self.multiTruth == i for i in range(len(self.classes))],
                self.classes,
                self.weights,
                bins,
                self.outDir
                + f"/Plots/{self.plotOutputName}_MultiClass_{self.config['variables'][var]['fileName']}_{self.sett}.pdf",
                xlog=self.config["variables"][var]["xlog"],
                ylog=self.config["variables"][var]["ylog"],
                xlabel=self.config["variables"][var]["plotName"],
                text=self.text + "\n" + self.config["dataset"],
            )


def getBins(binConfig: dict) -> np.ndarray:
    if binConfig["type"] == "lin":
        bins = np.linspace(binConfig["low"], binConfig["high"], binConfig["nBins"] + 1)
    elif binConfig["type"] == "log":
        bins = np.logspace(
            np.log10(binConfig["low"]),
            np.log10(binConfig["high"]),
            binConfig["nBins"] + 1,
        )
    else:
        raise ValueError(f"Type {binConfig['type']} not supported for binning!")

    if "additional" in binConfig:
        if type(binConfig["additional"]) == list:
            bins = np.concatenate([bins, np.array(binConfig["additional"])])

    return bins


def downsample(
    config: dict, fileName: str, sett: str, etBins: np.ndarray, factor: float = 1
) -> np.ndarray:
    """
    Downsamples signal to not have (factor x) more events in Et bins than bkg.

    Args
    ----
        fileName: *str*
            Name of the .h5 file where the data is stored.
        sett: *str*
            Which dataset to downsample: train, val, or test
        etBins: numpy.array
            Bins in which the signal should be downsampled to the bkg.

    """
    # Load the necessary data
    with h5py.File(fileName, "r") as hf:
        et = hf[config["VariableNames"]["et"]][:] / 1e3
        eta = np.abs(hf[config["VariableNames"]["eta"]][:])
        iff = hf[config["VariableNames"]["iff"]][:]

    # Get bin numbers for the data
    binNumbersEt = np.digitize(et, etBins) - 1

    indices = []

    for i in range(len(etBins) - 1):
        # Get mask for signal and background
        masks = {
            "Signal": ((binNumbersEt == i) & (iff == 2) & (eta < 2.47)),
            "Background": ((binNumbersEt == i) & (iff != 2) & (eta < 2.47))
            & ((iff != 7) & (iff != 0) & (iff != 1) & (iff != 4) & (iff != 6)),
        }

        if config["removeCFs"]:
            masks["Background"] = (masks["Background"] == 1) & (iff != 3)

        # Get indices of Signal and Background
        sigIndices = np.argwhere(masks["Signal"] == 1).flatten()
        bkgIndices = np.argwhere((masks["Background"] == 1)).flatten()

        # Get Number of Signal and Background events
        nSig = len(sigIndices)
        nBkg = len(bkgIndices)

        # If there is less signal than five times the background, keep all data
        if nSig < factor * nBkg:
            sigRandom = np.sort(np.random.choice(sigIndices, nSig, replace=False))

        # Else remove signal that there is five times as much signal as bkg
        else:
            sigRandom = np.sort(
                np.random.choice(sigIndices, int(factor * nBkg), replace=False)
            )

        bkgRandom = np.sort(np.random.choice(bkgIndices, nBkg, replace=False))
        # Collect signal and background indices to keep
        indices += list(sigRandom)
        indices += list(bkgRandom)

    # Create mask to keep only the wanted events
    mask = np.zeros(et.shape[0], dtype=np.int)
    mask[np.sort(indices)] = 1

    return mask


class FileSplitterSaver:
    def __init__(self, config: dict, inputFile: str, sett: str, mask: np.ndarray):
        self.config = config
        self.inputFile = inputFile
        self.outDir = self.config["outDir"]
        self.fileDir = self.outDir + "/Files"
        mkdir(self.outDir)
        mkdir(self.fileDir)
        self.outFileName = self.config["outFileName"]
        self.split = self.config["splitDownsampled"]
        self.sett = sett
        self.mask = mask

    def saveFiles(self) -> None:
        with h5py.File(self.inputFile, "r") as hf:
            self.keyList = list(hf.keys())

        nPointsPerFile = int(np.sum(self.mask) / self.split) + 1
        self.outFileNamesList = []
        for i, key in enumerate(self.keyList):
            with h5py.File(self.inputFile, "r") as hf:
                var = hf[f"{key}"][:]
            for n in range(self.split):
                if i == 0:
                    self.outFileNamesList.append(
                        self.fileDir + f"/{self.outFileName}_{self.sett}_{n:04d}.h5"
                    )
                saveVar = var[self.mask == 1][
                    n * nPointsPerFile : (n + 1) * nPointsPerFile
                ]

                with h5py.File(self.outFileNamesList[n], "w" if i == 0 else "a") as hf:
                    hf.create_dataset(key, data=saveVar, compression="lzf")

    def makeVDSFile(self) -> str:
        shapes = np.ones(self.split)
        # iterate over the input files to get the shapes of every one of them
        for i, infile in enumerate(self.outFileNamesList):
            with h5py.File(infile, "r") as hf:
                shapes[i] = hf[self.keyList[0]].shape[0]

        cumsum = np.cumsum(shapes)
        cumsum = np.concatenate((np.array([0]), cumsum))

        # iterate over the single datasets
        for j, var in enumerate(self.keyList):
            # create what will become the virtual dataset
            layout = h5py.VirtualLayout(shape=(int(np.sum(shapes)),))

            # iterate over the input files filling the virtual dataset
            for n, infile in enumerate(self.outFileNamesList):
                vsource = h5py.VirtualSource(infile, f"{var}", shape=(int(shapes[n]),))
                layout[int(cumsum[n]) : int(cumsum[n + 1])] = vsource

            # create the dataset in the output file.
            # If first time here create the output file
            with h5py.File(
                self.outDir + f"/VDS_{self.outFileName}_{self.sett}.h5",
                "w" if j == 0 else "a",
                libver="latest",
            ) as f:
                f.create_virtual_dataset(f"{var}", layout)

        return self.outDir + f"/VDS_{self.outFileName}_{self.sett}.h5"


def getClassWeights(fileName: str, weights: np.ndarray) -> np.ndarray:
    """
    Get weights for each class such that all have the same sum of weights.

    Args
    ----
        fileName: *str*
            Name of the .h5 file where the data is stored.
        sett: *str*
            Which dataset to downsample: train, val, or test
        downsamplingMask: numpy.array
            Array masking all events that are already downsampled since the
            class weights should make classes have the same sum of weights
            after downsampling and per sample weights.
        weights: numpy.array
            Array giving the per sample weights since the
            class weights should make classes have the same sum of weights
            after downsampling and per sample weights.

    Returns
    -------
        classWeights: *dict*
            Dictionary holding class weights for the single classes. The
            classes are encoded as integers as they are in a multi class
            training.

    """
    with h5py.File(fileName, "r") as hf:
        iff = hf[config["VariableNames"]["iff"]][:]
        tt = hf[config["VariableNames"]["truthType"]][:]
    classMasks = {
        "Signal": (iff == 2),
        "ChargeFlip": (iff == 3),
        "PhotonConv": (iff == 5),
        "HeavyFlavor": ((iff == 8) | (iff == 9)),
        "LFEgamma": ((iff == 10) & ((tt == 4) | (tt == 16))),
        "LFHadron": ((iff == 10) & (tt == 17)),
    }

    norm = np.sum(weights[classMasks["Signal"] == 1])
    classWeights = {}
    classWeights[0] = 1
    classWeights[1] = norm / np.sum(weights[classMasks["ChargeFlip"] == 1])
    classWeights[2] = norm / np.sum(weights[classMasks["PhotonConv"] == 1])
    classWeights[3] = norm / np.sum(weights[classMasks["HeavyFlavor"] == 1])
    classWeights[4] = norm / np.sum(weights[classMasks["LFEgamma"] == 1])
    classWeights[5] = norm / np.sum(weights[classMasks["LFHadron"] == 1])

    return classWeights


def reweight(
    config: dict,
    fileName: str,
    etBins: np.ndarray,
    refHist: np.ndarray,
    etaBins: np.ndarray,
):
    """
    Reweights each of the single classes to a common shape in Et and eta.

    The Et shape is taken from LFHadron bkg from a ttbar sample and is given
    as a histogram below. Therefore, if the binning in Et is changed, this
    histogram would need to change as well.
    The eta shape is just flat.

    Args
    ----
        fileName: *str*
            Name of the .h5 file where the data is stored.

    Returns
    -------
        fullW: numpy.array
            Calculated weights to give desired shape in Et and eta
        classWeightsInc: numpy.array
            Same weights as fullW but additionally multiplied with a class
            weight such that each class has the same sum of weights. This
            should be used for multiclass trainings (not for binary).

    """
    with h5py.File(fileName, "r") as hf:
        et = hf[config["VariableNames"]["et"]][:] * 1e-3
        eta = np.abs(hf[config["VariableNames"]["eta"]][:])
        iff = hf[config["VariableNames"]["iff"]][:]
        tt = hf[config["VariableNames"]["truthType"]][:]

    classMasks = {
        "Sig": (iff == 2),
        "CF": (iff == 3),
        "Phot": (iff == 5),
        "HF": ((iff == 8) | (iff == 9)),
        "LFEg": ((iff == 10) & ((tt == 4) | (tt == 16))),
        "LFH": ((iff == 10) & (tt == 17)),
    }

    weightHists = {}
    for key in classMasks:
        weightHists[key] = (
            refHist / np.histogram(et[classMasks[key] == 1], bins=etBins)[0]
        )

    weights = np.ones(et.shape)

    # Assign each data point the weight of its corresponding et-eta bin
    for key in classMasks:
        for i in range(len(etBins) - 1):
            binMask = (et > etBins[i]) & (et <= etBins[i + 1])
            weights[(classMasks[key] == 1) & (binMask == 1)] = weightHists[key][i]

        weights[classMasks[key] == 1] /= np.mean(weights[classMasks[key] == 1])

    etaHist = np.ones(etaBins.shape[0] - 1)

    fullW = np.ones(et.shape)

    weightHists = {}
    for key in classMasks:
        weightHists[key] = (
            etaHist / np.histogram(eta[classMasks[key] == 1], bins=etaBins)[0]
        )

    for key in classMasks:
        for i in range(len(etaBins) - 1):
            binMask = (eta > etaBins[i]) & (eta <= etaBins[i + 1])
            fullW[(classMasks[key] == 1) & (binMask == 1)] = (
                weightHists[key][i] * weights[(classMasks[key] == 1) & (binMask == 1)]
            )

        fullW[(np.isnan(fullW)) | (np.isinf(fullW))] = 0
        fullW[classMasks[key] == 1] /= np.mean(fullW[classMasks[key] == 1])

    fullW[classMasks["Sig"] == 1] *= (
        np.sum(fullW[classMasks["CF"] == 1])
        + np.sum(fullW[classMasks["Phot"] == 1])
        + np.sum(fullW[classMasks["HF"] == 1])
        + np.sum(fullW[classMasks["LFEg"] == 1])
        + np.sum(fullW[classMasks["LFH"] == 1])
    ) / np.sum(fullW[classMasks["Sig"] == 1])

    # Normalize the weights of the downsampled weights
    fullW /= np.mean(fullW)

    classWeightsInc = copy.deepcopy(fullW)
    for key in classMasks:
        if key == "Sig":
            continue

        classWeightsInc[classMasks[key] == 1] *= np.sum(
            classWeightsInc[classMasks["Sig"] == 1]
        ) / np.sum(classWeightsInc[classMasks[key] == 1])

    classWeightsInc /= np.mean(classWeightsInc)

    return fullW, classWeightsInc


class SaveAdditionalVariableToFiles:
    def __init__(self, fileName: str, varName: str, varArray: np.ndarray):
        self.fileName = fileName
        self.varName = varName
        self.varArray = varArray

    def saveToFiles(self) -> None:
        self.getOriginalFiles()
        self.fillOriginalFiles()
        self.fillVDSFile()

    def getOriginalFiles(self) -> None:
        # Get the original files so that the weights can be saved there
        with h5py.File(self.fileName, "r") as hf:
            self.key = list(hf.keys())[0]
            self.virtualSources = hf[self.key].virtual_sources()

    def fillOriginalFiles(self) -> None:
        # Loop over the original files and save the weights
        filledPoints = 0
        self.shapes = np.zeros(len(self.virtualSources))
        for i in range(len(self.virtualSources)):
            with h5py.File(self.virtualSources[i][1], "a") as hf:
                self.shapes[i] = int(hf[self.key].shape[0])
                hf.create_dataset(
                    self.varName,
                    data=self.varArray[
                        int(filledPoints) : int(filledPoints + self.shapes[i])
                    ],
                    compression="lzf",
                )
            filledPoints += self.shapes[i]

    def fillVDSFile(self) -> None:
        # Add the weights as virtual datasets
        cumsum = np.cumsum(self.shapes)
        cumsum = np.concatenate((np.array([0]), cumsum))

        layout = h5py.VirtualLayout(shape=(int(np.sum(self.shapes)),))

        # iterate over the input files filling the virtual dataset
        for i in range(len(self.virtualSources)):
            vsource = h5py.VirtualSource(
                self.virtualSources[i][1], self.varName, shape=(int(self.shapes[i]),)
            )
            layout[int(cumsum[i]) : int(cumsum[i + 1])] = vsource
        # create the dataset in the output file, since the file already exists
        # always append
        with h5py.File(self.fileName, "a", libver="latest") as f:
            f.create_virtual_dataset(self.varName, layout)
