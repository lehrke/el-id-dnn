"""Combine several .h5 files into one file with virtual datasets."""
import h5py
import numpy as np
import argparse


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = argparse.ArgumentParser(
        description="Options for creating a "
        + "combined h5 file with "
        + "virtual datasets."
    )

    parser.add_argument(
        "--outFile",
        action="store",
        type=str,
        default="VDS.h5",
        help="Save file with virtual datasets here.",
    )
    parser.add_argument(
        "--inputFiles",
        action="store",
        type=str,
        nargs="+",
        help="Input files which should be transformed. Each "
        + "file will be saved into one protobuf file.",
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = getParser()

    # Get the name of all datasets from the first input file
    with h5py.File(args.inputFiles[0], "r") as hf:
        keys = [key for key in hf["train"].keys()]

    # iterate over train, val, and test set
    for k, sett in enumerate(["train", "val", "test"]):
        shapes = np.ones(len(args.inputFiles))
        # iterate over the input files to get the shapes of every one of them
        for i, infile in enumerate(args.inputFiles):
            with h5py.File(infile, "r") as hf:
                shapes[i] = hf[f"{sett}/{keys[0]}"].shape[0]

        cumsum = np.cumsum(shapes)
        cumsum = np.concatenate((np.array([0]), cumsum))

        # iterate over the single datasets
        for j, var in enumerate(keys):
            # create what will become the virtual dataset
            layout = h5py.VirtualLayout(shape=(int(np.sum(shapes)),))

            # iterate over the input files filling the virtual dataset
            for n, infile in enumerate(args.inputFiles):
                vsource = h5py.VirtualSource(
                    infile, f"{sett}/{var}", shape=(int(shapes[n]),)
                )
                layout[int(cumsum[n]) : int(cumsum[n + 1])] = vsource

            # create the dataset in the output file.
            # If first time here create the output file
            if k == 0 and j == 0:
                with h5py.File(args.outFile, "w", libver="latest") as f:
                    f.create_virtual_dataset(f"{sett}/{var}", layout)
            else:
                with h5py.File(args.outFile, "a", libver="latest") as f:
                    f.create_virtual_dataset(f"{sett}/{var}", layout)
