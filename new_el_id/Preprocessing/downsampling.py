"""Downsample one file to the same distribution in Et, eta for sig and bkg."""
import numpy as np
import argparse

from new_el_id.CommonTools import Configuration, mkdir, CustomParser
from new_el_id.Preprocessing import (
    PreprocessingPlotting,
    downsample,
    FileSplitterSaver,
    getBins,
)


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = CustomParser(description="Options for downsampling")

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = getParser()
    configLoader = Configuration(args.configFile, preprocessing=True)
    configLoader.replaceWithCommandLineArguments(args)
    config = configLoader.config

    downsamplingBins = getBins(config["downsamplingBins"])
    for key in config["split"]:
        inputFile = (
            config["outDirConversion"]
            + f"/VDS_{config['outFileNameConversion']}_{key}.h5"
        )

        mask = downsample(
            config,
            inputFile,
            key,
            downsamplingBins,
            factor=config["downsamplingFactor"],
        )

        saver = FileSplitterSaver(config, inputFile, key, mask)
        saver.saveFiles()
        vdsFileName = saver.makeVDSFile()

        plotter = PreprocessingPlotting(
            config, vdsFileName, key, weights=None, downsampling=True
        )
        plotter.plotVars()
