"""Reweight the downsampled eta and et distributions to a desired shape."""
import h5py
import glob
import argparse

from new_el_id.CommonTools import Configuration, CustomParser
from new_el_id.Preprocessing import (
    PreprocessingPlotting,
    reweight,
    SaveAdditionalVariableToFiles,
    getBins,
)


def getParser() -> argparse.Namespace:
    """Get options set in command line."""
    parser = CustomParser(description="Options for reweighting")

    args = parser.parse_args()

    return args


if __name__ == "__main__":
    args = getParser()
    configLoader = Configuration(args.configFile, preprocessing=True)
    configLoader.replaceWithCommandLineArguments(args)
    config = configLoader.config

    inputFiles = glob.glob(config["outDir"] + f"/*{config['outFileName']}*.h5")
    with h5py.File(inputFiles[0], "r") as hf:
        keys = list(hf.keys())

    calcWeights = True
    if "weight" in keys:
        print("Weights are already saved in the files. Only doing plotting")
        calcWeights = False

    weightingBinsET = getBins(config["weightingBinsET"])
    refHistET = config["weightingHistET"]
    weightingBinsAbsEta = getBins(config["weightingBinsAbsEta"])

    for file in inputFiles:
        if calcWeights:
            # Calculate actual weights and save them
            weights, withClassWeights = reweight(
                config, file, weightingBinsET, refHistET, weightingBinsAbsEta
            )

            weightSaver = SaveAdditionalVariableToFiles(file, "weight", weights)
            weightSaver.saveToFiles()
            classWeightSaver = SaveAdditionalVariableToFiles(
                file, "weightIncClassWeights", withClassWeights
            )
            classWeightSaver.saveToFiles()

        sett = file.split("/")[-1].split(".")[0].split("_")[-1]
        plotter = PreprocessingPlotting(
            config, file, sett, weights="weight", weighting=True
        )
        plotter.plotVars()

        plotter = PreprocessingPlotting(
            config, file, sett, weights="weightIncClassWeights", classWeighting=True
        )
        plotter.plotVars()
